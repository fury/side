#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' -D{tests,yelp_manual}=false '
    ])

def setup():
	pisitools.dosed("meson.build", "^subdir\(\'help\'\)", deleteLine = True)
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
