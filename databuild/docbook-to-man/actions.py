#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, autotools, get

def setup():
	shelltools.system("chmod +w -R .")
	shelltools.system("find -name Makefile -exec sed -i 's/^ROOT =.*//' '{}' \;")
	shelltools.system("sed -ri 's:^(ROOT=).*:\1/usr:' cmd/docbook-to-man.sh")

def build():
	autotools.make()

def install():
	pisitools.dodir("/usr/bin")
	pisitools.dodir("/usr/lib")
	pisitools.dodir("/usr/share")
	autotools.rawInstall("ROOT=%s/usr" % get.installDIR())
