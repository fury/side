#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

dest = "/usr/share/xml/docbook/xsl-stylesheets"

def install():
	autotools.rawInstall("-f Makefile_ DESTDIR=%s%s" % (get.installDIR(), dest))

	pisitools.insinto(dest, "VERSION.xsl")
