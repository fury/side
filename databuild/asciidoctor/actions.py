#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, docopyright, get

name = "asciidoctor-%s" % get.srcVERSION()

i = ''.join([
    ' --ignore-dependencies',
    ' --no-user-install',
    ' --verbose',
    ' --no-document',
    ' --install-dir %s' % get.installDIR(),
    ' -i %s/usr/lib/ruby/gems/3.3.0/' % get.installDIR(),
    ' -n %s/usr/bin %s.gem ' % (get.installDIR(), name)
    ])

def build():
	shelltools.system("gem build asciidoctor.gemspec")

def install():
	shelltools.system("gem install %s %s.gem" % (i, name))

	pisitools.doman("man/asciidoctor.1")

	for dirs__t in ["build_info", "cache", "doc", "extensions", "plugins"]:
		pisitools.removeDir("/usr/lib/ruby/gems/3.3.0/%s" % dirs__t)
	pisitools.removeDir("/usr/lib/ruby/gems/3.3.0/gems/%s/data" % name)
	pisitools.removeDir("/usr/lib/ruby/gems/3.3.0/gems/%s/man" % name)
	pisitools.remove("/usr/lib/ruby/gems/3.3.0/gems/%s/asciidoctor.gemspec" % name)
	pisitools.remove("/usr/lib/ruby/gems/3.3.0/gems/%s/*.adoc" % name)
	pisitools.remove("/usr/lib/ruby/gems/3.3.0/gems/%s/LICENSE" % name)

	docopyright.installCopyright()
