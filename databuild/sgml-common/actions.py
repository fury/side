#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

def setup():
	shelltools.unlink("mkinstalldirs")
	shelltools.system("sed -i 's/HTML//' doc/Makefile.am")
	shelltools.system("echo 'man_MANS = install-catalog.8' > doc/man/Makefile.am")
	autotools.autoreconf("-fi")
	autotools.configure("--prefix=/usr --sysconfdir=/etc")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
