#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, cmaketools, autotools, get

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -Duse_sys_{spdlog,sqlite3}=ON',
    ' -Dbuild_{wizard,doc}=OFF',
    ' -Bbuild -G "Unix Makefiles" -L '
    ])

def setup():
	shelltools.system("grep -rl '^#!.*python$' | xargs sed -i '1s/python/&3/'")
	cmaketools.configure(i)

def build():
	autotools.make("-C build")

def install():
	autotools.rawInstall("-C build DESTDIR=%s" % get.installDIR())
