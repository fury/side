#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, pisitools

def build():
	pythonmodules.run("bootstrap.py")
	pythonmodules.compile()

def install():
	pythonmodules.install()

	pisitools.rename("/usr/bin/easy_install", "py2easy-install")
	pisitools.remove("/usr/lib/python2.7/site-packages/setuptools/*.exe")
