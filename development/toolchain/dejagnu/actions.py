#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

def setup():
	shelltools.makedirs("build")
	shelltools.cd("build")
	shelltools.system("../configure --prefix=/usr")

def build():
	pass

def install():
	shelltools.cd("build")
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
