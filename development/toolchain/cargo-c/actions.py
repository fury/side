#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, get, docopyright

def setup():
	shelltools.system("cargo fetch --manifest-path Cargo.toml")

def build():
	shelltools.system("cargo build --release --manifest-path Cargo.toml")

def install():
	i = ''.join([
	' --offline',
	' --frozen',
	' --no-track',
	' --path . --root=%s/usr' % get.installDIR()
	])
	shelltools.system("cargo install %s" % i)

	docopyright.installCopyright()
