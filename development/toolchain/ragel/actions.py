#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	pisitools.dosed("configure.ac", "withval\/share", "withval/share/colm")
	pisitools.dosed("Makefile.am", "dist_doc_DATA", deleteLine = True)
	pisitools.dosed("src/Makefile.am", "data_DATA", "pkgdata_DATA")
	pisitools.dosed("src/host-go/Makefile.am", "data_DATA", "pkgdata_DATA")
	autotools.autoreconf("-vif")
	autotools.configure("--prefix=/usr --with-colm=/usr --disable-manual --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/usr/share/vim/vim91/syntax/", "ragel.vim")
