#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

shelltools.export("GOROOT", "/usr/lib/go")

def build():
	autotools.make("BUILDTAGS='seccomp no_btrfs'")
	autotools.make("man")

def install():
	autotools.rawInstall("PREFIX=/usr DESTDIR=%s install-man" % get.installDIR())

	a = "%s/usr/bin/containerd config default" % get.installDIR()
	b = "%s/etc/containerd/config.toml" % get.installDIR()

	pisitools.dodir("/etc/containerd")
	shelltools.system("%s > %s" % (a, b))
