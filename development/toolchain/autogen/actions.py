#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --disable-static',
    ' --disable-dependency-tracking '
    ])

def setup():
	pisitools.cflags.add("-Wno-error=format-overflow")
	pisitools.dosed("configure", "\ -Werror\ ", " ")
	# automake-1.17 compatible
	pisitools.dosed("configure", "1.16", "1.17")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
