#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, pisitools, mesontools

i = ''.join([
    ' -DCAMKE_BUILD_TYPE=Release',
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DBUILD_HTML_DOCS=OFF',
    ' -DBUILD_TESTING=OFF',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	pisitools.dosed("kde-modules/KDEInstallDirsCommon.cmake", "lib64", "lib")
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
