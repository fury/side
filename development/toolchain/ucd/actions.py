#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools

WorkDir = "."

def install():
	for t in shelltools.ls("."):
		pisitools.insinto("/usr/share/unicode/ucd", t)
