#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

a = ''.join([
    ' INSTALL_TOP=%s/usr' % get.installDIR(),
    ' INSTALL_DATA="cp -d"',
    ' INSTALL_MAN=%s/usr/share/man/man1' % get.installDIR(),
    ' TO_LIB="liblua.so liblua.so.5.4 liblua.so.5.4.7"'
    ])

def build():
	autotools.make("linux")

def install():
	autotools.rawInstall(a)
