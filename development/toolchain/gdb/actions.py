#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --disable-nls',
    ' --enable-host-shared',
    ' --with-zstd=no',
    ' --with-system-zlib',
    ' --with-system-readline',
    ' --with-python=/usr/bin/python3 '
    ])

def setup():
	shelltools.makedirs("build")
	shelltools.cd("build")
	shelltools.system("../configure %s" % i)

def build():
	autotools.make("-C build")

def install():
	autotools.rawInstall("-C build DESTDIR=%s" % get.installDIR())

	# those are provided by binutils
	pisitools.removeDir("/usr/include")
	pisitools.remove("/usr/lib/lib*.a")
	for t in ["bfd.info", "ctf-spec.info", "sframe-spec.info"]:
		pisitools.remove("/usr/share/info/%s" % t)
