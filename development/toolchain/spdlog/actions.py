#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DSPDLOG_BUILD_SHARED=ON',
    ' -DSPDLOG_FMT_EXTERNAL=ON',
    ' -DSPDLOG_BUILD_EXAMPLE=OFF',
    ' -Bbuild -G Ninja -L '
    ])

a = "#define SPDLOG_FMT_EXTERNAL"

def setup():
	shelltools.system("sed -i 's:^.*%s:%s:' include/spdlog/tweakme.h" % (a, a))
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
