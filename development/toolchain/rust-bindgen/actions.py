#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, docopyright

def build():
	shelltools.system("cargo build --release")

def install():
	pisitools.dobin("target/release/bindgen")

	docopyright.installCopyright()
