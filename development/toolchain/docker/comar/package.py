#!/usr/bin/python2

import getpass, os, re

OUR_NAME = "docker"

logfile = "/var/log/docker.log"

def postInstall(fromVersion, fromRelease, toVersion, toRelease):
    try:
        os.system("/usr/sbin/groupadd %s" % OUR_NAME)
        os.system("/usr/sbin/usermod -a -G docker %s" % getpass.getuser())
        os.system("/usr/bin/touch %s" % logfile)
        os.system("/usr/bin/chown root:docker %s" % logfile)
        os.system("/usr/bin/chmod 0644 %s" % logfile)
    except:
        pass

def preRemove():
    try:
        os.system("/usr/sbin/groupdel %s" % OUR_NAME)
    except:
        pass
