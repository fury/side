from comar.service import *
import os

serviceType = "local"
serviceDefault = "off"
serviceDesc = _({"en": "Docker Management Service"})
serviceConf = "docker"

pidfile = "/var/run/docker.pid"

@synchronized
def start():
  os.environ["PATH"] = "/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/sbin:/usr/local/bin"
  os.system("/usr/sbin/modprobe -va bridge nf_nat br_netfilter")

  startService(command="/usr/bin/dockerd",
               args="%s" % config.get("DOCKER_OPTS"),
               pidfile=pidfile,
               detach=True,
               donotify=True)

@synchronized
def stop():
  stopService(command="/usr/bin/dockerd",
              donotify=True)

def status():
  return isServiceRunning(pidfile)
