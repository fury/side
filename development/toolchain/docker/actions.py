#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, get, docopyright

shelltools.export("GOROOT", "/usr/lib/go")
shelltools.export("GOPATH", get.workDIR())
shelltools.export("VERSION", get.srcVERSION())
shelltools.export("GITCOMMIT", "fca702de7f71362c8d103073c7e4a1d0a467fadd")
shelltools.export("DOCKER_GITCOMMIT", "293681613032e6d1a39cc88115847d3984195c24")
shelltools.export("DOCKER_BUILDTAGS", "seccomp no_btrfs")
shelltools.export("DISABLE_WARN_OUTSIDE_CONTAINER", "1")

def setup():
	a = "%s/src/github.com/docker/" % get.workDIR()
	for b in ["cli", "docker"]:
		shelltools.makedirs("%s%s" % (a, b))

	shelltools.cd("%scli" % a)
	shelltools.system("ln -s ../../../../cli-%s/* ." % get.srcVERSION())

	shelltools.cd("%sdocker" % a)
	shelltools.system("ln -s ../../../../moby-%s/* ." % get.srcVERSION())

def build():
	shelltools.cd("%s/moby-%s" % (get.workDIR(), get.srcVERSION()))
	shelltools.system("hack/make.sh dynbinary")

	shelltools.cd("%s/cli-%s" % (get.workDIR(), get.srcVERSION()))
	shelltools.system("make dynbinary")
	shelltools.system("make manpages")

def install():
	shelltools.cd("%s/cli-%s" % (get.workDIR(), get.srcVERSION()))
	pisitools.dobin("build/docker-linux-amd64")
	pisitools.dosym("docker-linux-amd64", "/usr/bin/docker")
	pisitools.insinto("/usr/share/bash-completion/completions", "contrib/completion/bash/docker")
	docopyright.installCopyright()
	for i in ["1", "5", "8"]:
		shelltools.cd("man/man%s" % i)
		pisitools.doman("*")
		shelltools.cd("../..")

	shelltools.cd("%s/moby-%s" % (get.workDIR(), get.srcVERSION()))
	pisitools.dobin("bundles/dynbinary-daemon/docker-proxy")
	pisitools.dobin("bundles/dynbinary-daemon/dockerd")
	pisitools.dobin("contrib/check-config.sh")
	pisitools.insinto("/usr/lib/udev/rules.d", "contrib/udev/80-docker.rules")
