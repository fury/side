#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pisitools, shelltools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --system-libs',
    ' --sphinx-man',
    ' --parallel=%s' % get.makeJOBS().replace("-j", ""),
    ' --mandir=/share/man',
    ' --docdir=/share/copyright/cmake',
    ' --no-system-{cppdap,librhash}',
    ])

def setup():
	pisitools.dosed("Modules/GNUInstallDirs.cmake", "\"lib64\"", "\"lib\"")
	shelltools.system("./bootstrap %s" % i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	l = "Utilities/cmcppdap/LICENSE"
	pisitools.insinto("/usr/share/copyright/cmake/cmcppdap", l)
	pisitools.removeDir("/usr/share/vim")
