#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, scons, get

def setup():
	shelltools.system("sed -i '/Append/s:RPATH=libdir,::' SConstruct")
	shelltools.system("sed -i '/Default/s:lib_static,::' SConstruct")
	shelltools.system("sed -i '/Alias/s:install_static,::' SConstruct")

def build():
	scons.make("PREFIX=/usr")

def install():
	scons.install("PREFIX=%s/usr install" % get.installDIR())
