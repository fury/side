#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -D{perl,python}=enabled',
    ' -Dgitconfig=/etc/gitconfig',
    ' -Ddefault_editor=vim',
    ' -Dhttps_backend=openssl',
    ' -Dgitweb=disabled',
    ' -Dgettext=disabled',
    ' -Ddocs=man '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	a = "/usr/share/bash-completion/completions"
	b = "contrib/completion/git-completion.bash"
	pisitools.insinto(a, b, "git")
