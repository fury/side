#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-system-zlib',
    ' --enable-default-{pie,ssp}',
    ' --disable-{fixincludes,nls,multilib}',
    ' --enable-languages=c,c++,fortran,objc,obj-c++,go',
    ])

def setup():
	shelltools.system("sed -i.orig '/m64=/s/lib64/lib/' gcc/config/i386/t-linux64")
	shelltools.makedirs("build")
	shelltools.cd("build")
	shelltools.system("../configure %s" % i)

def build():
	shelltools.cd("build")
	autotools.make()

def check():
#	shelltools.cd("build")
#	shelltools.system("ulimit -s 32768")
#	autotools.make("-k check")
	pass

def install():
	shelltools.cd("build")
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dosym("/usr/bin/cpp", "/usr/lib/cpp")
	pisitools.dosym("gcc", "/usr/bin/cc")
	pisitools.dosym("x86_64-pc-linux-gnu-gcc-ar", "/usr/bin/x86_64-pc-linux-gnu-ar")
	pisitools.dosym("/usr/libexec/gcc/x86_64-pc-linux-gnu/13.3.0/liblto_plugin.so", "/usr/lib/bfd-plugins/liblto_plugin.so")

	gdbpy_dir = "/usr/share/gdb/auto-load/usr/lib/"
	pisitools.dodir(gdbpy_dir)

	files = shelltools.ls("%s/usr/lib" % get.installDIR())
	for i in files:
		if i.endswith("gdb.py"):
			pisitools.domove("/usr/lib/%s" % i, gdbpy_dir)

	# install copyleft
	for c in ["COPYING", "COPYING3", "COPYING.LIB", "COPYING3.LIB", "COPYING.RUNTIME", "MAINTAINERS"]:
		pisitools.insinto("/usr/share/copyright/gcc", "../%s" % c)
