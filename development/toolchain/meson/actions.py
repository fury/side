#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, pisitools

def build():
	pythonmodules.compile(pyVer = "3")

def install():
	pythonmodules.install(pyVer = "3")

	vimfile = "data/syntax-highlighting/vim/ftdetect"
	pisitools.insinto("/usr/share/vim/vim91", vimfile)

	cmpl = "data/shell-completions/bash/meson"
	pisitools.insinto("/usr/share/bash-completion/completions", cmpl)
