#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

def setup():
	shelltools.export("CFLAGS", get.CFLAGS().replace("-fstack-protector", ""))
	autotools.configure("--prefix=/usr --without-mpicc")

def build():
	autotools.make()

def check():
	pass

def install():
	autotools.rawInstall("DESTDIR=%s BUILD_ALL_DOCS=no" % get.installDIR())
