#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DSPIRV_WERROR=OFF',
    ' -DBUILD_SHARED_LIBS=ON',
    ' -DSPIRV_TOOLS_BUILD_STATIC=OFF',
    ' -DSPIRV-Headers_SOURCE_DIR=/usr',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
