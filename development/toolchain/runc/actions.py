#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, docopyright

def build():
	shelltools.export("GOROOT", "/usr/lib/go")
	shelltools.system("make")
	shelltools.system("make man")

def install():
	pisitools.dobin("runc")
	pisitools.insinto("/usr/share/bash-completion/completions", "contrib/completions/bash/runc")
	pisitools.doman("man/man8/*")

	docopyright.installCopyright()
