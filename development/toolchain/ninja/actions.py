#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools

i = ''.join([
'/int Guess/a int   j = 0; ',
'char* jobs = getenv( "NINJAJOBS" ); ',
'if ( jobs != NULL ) j = atoi( jobs ); ',
'if ( j > 0 ) return j;'
])

def build():
	# allow NINJAJOBS variable recognition
	shelltools.system("sed -i '%s' src/ninja.cc" % i)

	shelltools.system("python3 configure.py --bootstrap")
	shelltools.system("asciidoctor -b manpage -d manpage -o ninja.1 doc/manual.asciidoc")

def check():
	pass

def install():
	pisitools.dobin("ninja", "/usr/bin")
	pisitools.insinto("/usr/share/bash-completion/completions", "misc/bash-completion", "ninja")

	pisitools.doman("ninja.1")
