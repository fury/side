#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-{perl,python3}=yes',
    ' --disable-sharp',
    ' --disable-go',
    ' --disable-java',
    ' --disable-lua',
    ' --disable-php',
    ' --disable-tcl',
    ' --disable-ruby',
    ' --disable-static',
    ' --with-libgd=no',
    ' --with-glut=no',
    ' --with-gtk=no',
    ' --with-gdk-pixbuf=no',
    ' --with-qt=no',
    ' --with-webp=no',
    ' --with-rsvg=no',
    ' --with-poppler=no',
    ' --with-ghostscript=no',
    ' --with-glade=no '
    ])

def setup():
	shelltools.export("LIBPOSTFIX", "/")
	shelltools.system("sh autogen.sh")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.removeDir("/usr/share/doc")
