#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools, shelltools, pisitools, get

WorkDir = "llvm-project-%s.src/llvm" % get.srcVERSION()

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DLLVM_ENABLE_FFI=ON',
    ' -DLLVM_BUILD_LLVM_DYLIB=ON',
    ' -DLLVM_LINK_LLVM_DYLIB=ON',
    ' -DLLVM_ENABLE_RTTI=ON',
    ' -DLLVM_BINUTILS_INCDIR=/usr/include',
    ' -DLLVM_TARGETS_TO_BUILD="X86;AMDGPU"',
    ' -DLLVM_ENABLE_PROJECTS="clang;polly"',
    ' -DLLVM_INCLUDE_BENCHMARKS=OFF',
    ' -DCLANG_DEFAULT_PIE_ON_LINUX=ON',
    ' -DCLANG_CONFIG_FILE_SYSTEM_DIR=/etc/clang',
    ' -Wno-dev -Bbuild -G Ninja '
    ])

def setup():
	shelltools.system("grep -rl '#!.*python' | xargs sed -i '1s/python$/python3/'")
	shelltools.export("CC", "gcc")
	shelltools.export("CXX", "g++")
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	pisitools.dodir("/etc/clang")
	b = "-fstack-protector-strong"
	for a in ["clang", "clang++"]:
		shelltools.echo("%s/etc/clang/%s.cfg" % (get.installDIR(), a), b)
