#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, docopyright, get

def setup():
	dist_rs = "src/bootstrap/src/core/build_steps/dist.rs"
	pisitools.dosed(dist_rs, "etc/bash_completion.d", "share/bash-completion/completions")

def build():
	shelltools.export("LIBSSH2_SYS_USE_PKG_CONFIG", "1")
	shelltools.export("LIBSQLITE3_SYS_USE_PKG_CONFIG", "1")
	shelltools.export("RUST_BACKTRACE", "1")
	shelltools.system("python ./x.py build")

def install():
	for a in ["rustc std", "--stage=1 cargo clippy rustfmt"]:
		shelltools.system("DESTDIR=%s python ./x.py install %s" % (get.installDIR(), a))

	for b in ["zsh", "doc"]:
		pisitools.removeDir("/usr/share/%s" % b)
	docopyright.installCopyright()

	for i in shelltools.ls("%s/usr/lib/rustlib/" % get.installDIR()):
		if shelltools.isFile("%s/usr/lib/rustlib/%s" % (get.installDIR(), i)):
			pisitools.remove("/usr/lib/rustlib/%s" % i)
