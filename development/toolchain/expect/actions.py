#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-tcl=/usr/lib',
    ' --enable-shared',
    ' --with-tclinclude=/usr/include '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("test")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	pisitools.dosym("expect5.45.4/libexpect5.45.4.so", "/usr/lib/libexpect5.45.4.so")
