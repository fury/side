#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, get
import os

def build():
	shelltools.export("GOARCH", "amd64")
	shelltools.export("GOAMD64", "v1")
	shelltools.export("GOROOT_FINAL", "/usr/lib/go")
	shelltools.export("GOROOT_BOOTSTRAP", "/usr/lib/go")
	shelltools.cd("src")
	shelltools.system("./make.bash -v")

def install():
	pisitools.dobin("bin/go")
	pisitools.dobin("bin/gofmt")

	pisitools.insinto("/usr/lib/go", "lib")
	pisitools.insinto("/usr/lib/go", "pkg")

	dir__t = "src/"

	for dir__t, dirs, files in os.walk(dir__t):
		for f in files:
			if f.endswith(("test.go", ".bat", ".rc")):
				shelltools.unlink(os.path.join(dir__t, f))
		for d in dirs:
			if d.endswith("testdata"):
				shelltools.unlinkDir(os.path.join(dir__t, d))

	pisitools.insinto("/usr/lib/go", "src")

	for b in ["go", "gofmt"]:
		pisitools.dosym("/usr/bin/%s" % b, "/usr/lib/go/bin/%s" % b)

	pisitools.insinto("/usr/lib/go", "go.env")

	for l in ["LICENSE", "PATENTS"]:
		pisitools.insinto("/usr/share/copyright/go", l)
