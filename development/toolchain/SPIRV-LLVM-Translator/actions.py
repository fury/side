#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DBUILD_SHARED_LIBS=ON',
    ' -DCMAKE_SKIP_INSTALL_RPATH=ON',
    ' -DLLVM_EXTERNAL_SPIRV_HEADERS_SOURCE_DIR=/usr',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
