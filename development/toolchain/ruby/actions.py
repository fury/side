#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-shared',
    ' --disable-rpath',
    ' --disable-yjit',
    ' --with-mantype=man',
    ' --without-valgrind',
    ' --without-baseruby',
    ' ac_cv_func_qsort_r=no',
    ' optflags=\'-O2 -fno-fast-math\'',
    ' --with-rdoc=no '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("-j1 -k check")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
