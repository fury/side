#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, pisitools

def build():
	pythonmodules.makewhl()

def install():
	pythonmodules.installwhl()

	for i in ["scons-time.1", "scons.1", "sconsign.1",]:
		pisitools.doman(i)
