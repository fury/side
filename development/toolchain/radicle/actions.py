#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, docopyright, get

def setup():
	url = "https://seed.radicle.xyz/z3gqcJUoA1n9HaHKufZs5FCSGazv5.git"
	shelltools.system("git clone %s heartwood" % url)
	shelltools.cd("heartwood")
	shelltools.system("git checkout 70f0cc35")

def build():
	pass

def install():
	shelltools.cd("heartwood")
	for suff in ["cli", "node", "remote-helper"]:
		installParams = ''.join([
		' --path radicle-%s' % suff,
		' --force',
		' --locked',
		' --no-track',
		' --root %s/usr ' % get.installDIR()
		])
		shelltools.system("cargo install %s" % installParams)

	for l in ["git-remote-rad", "radicle-node", "rad", "rad-id", "rad-patch"]:
		shelltools.system("asciidoctor -b manpage -d manpage %s.1.adoc" % l)
		pisitools.doman("%s.1" % l)

	pisitools.insinto("/etc/bash_completion.d/", "../radicle.bash")
	docopyright.installCopyright()
