#!/usr/bin/python2
# -*- coding: utf-8 -*-

import os

SEEDDIR = "/var/lib/seed"

def postInstall(fromVersion, fromRelease, toVersion, toRelease):
	try:
		os.system("useradd -r -m -d %s -k /dev/null -s /usr/sbin/nologin -U seed" % SEEDDIR)
	except:
		pass

def postRemove():
	pass
