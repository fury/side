#!/usr/bin/python2
# -*- coding: utf-8 -*-

from comar.service import *

serviceType = "local"
serviceDesc = _({"en": "Local radicle node"})
serviceDefault = "off"
serviceConf = "radicle-node"

PIDFILE = "/run/radicle-node.pid"

@synchronized
def start():
    for radvar in ["RAD_HOME", "RUST_BACKTRACE", "RUST_LOG"]:
        os.environ[radvar] = "%s" % config.get(radvar)
    if "RAD_PASSPHRASE" in config:
        os.environ["RAD_PASSPHRASE"] = "%s" % config.get("RAD_PASSPHRASE")
    startService(command="/usr/bin/radicle-node",
                 args=" --listen 0.0.0.0:8776 --force ",
                 chuid="seed:seed",
                 pidfile=PIDFILE,
                 makepid=True,
                 detach=True,
                 donotify=True)

@synchronized
def stop():
    stopService(pidfile=PIDFILE, donotify=True)
    try:
        os.unlink(PIDFILE)
    except:
        pass

def status():
    return isServiceRunning(pidfile=PIDFILE)
