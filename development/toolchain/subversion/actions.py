#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get, perlmodules

i = ''.join([
    ' PYTHON=/usr/bin/python3',
    ' --prefix=/usr',
    ' --with-swig-{perl,python,ruby}',
    ' --disable-static '
    ])

def setup():
	pisitools.dosed("Makefile.in", "\$\(datadir\)\/pkgconfig", "$(libdir)/pkgconfig")
	autotools.configure(i)

def build():
	autotools.make()
	for l in ["swig-pl", "swig-py", "swig-rb"]:
		autotools.make(l)

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	for l in ["swig-pl", "swig-py", "swig-rb"]:
		autotools.rawInstall("DESTDIR=%s install-%s" % (get.installDIR(), l))

	perlmodules.removePodfiles()
