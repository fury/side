#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-program',
    ' --disable-manual',
    ' --disable-static '
    ])

def setup():
	pisitools.dosed("Makefile.am", "dist_doc_DATA", deleteLine = True)
	pisitools.dosed("test/Makefile.am", "data_DATA", "pkgdata_DATA")
	pisitools.dosed("src/cgil/Makefile.am", "data_DATA", "pkgdata_DATA")
	autotools.autoreconf("-vif")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/usr/share/vim/vim91/syntax/", "colm.vim")
