#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

def setup():
	autotools.configure("--prefix=/usr")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	# install copyright
	from pisi.actionsapi import shelltools, pisitools
	SkipList = [
               '.',
               'debugger',
               'devhelp',
               'geanylua',
               'geanypg',
               'geanypy',
               'markdown',
               'multiterm',
               'scope',
               'spellcheck',
               'webhelper'
               ]

	for a in shelltools.ls("."):
		if not a.startswith(tuple(SkipList)) and not shelltools.isFile(a):
			shelltools.cd(a)
			for b in shelltools.ls("."):
				if b.startswith(("AUTHORS", "COPYING", "THANKS")):
					try:
						destdir = "/usr/share/copyright/geany-plugins"
						pisitools.insinto(destdir, b, "%s-%s" % (a, b))
					except AttributeError, e:
						pass
			shelltools.cd("..")

	# clean up
	pisitools.removeDir("/usr/share/doc")
