#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	autotools.autoreconf("-fiv")
	autotools.configure()

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dodir("/etc/xdg/lxsession")
	pisitools.dosym("/etc/xdg/sde-session-manager/SDE", "/etc/xdg/lxsession/SDE")
	# system profile of waterline.
	pisitools.domove("/etc/xdg/sde", "/usr/share/sde-config", "sde")
