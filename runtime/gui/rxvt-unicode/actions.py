#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-xft',
    ' --enable-perl',
    ' --enable-256-color',
    ' --enable-everything '
    ])

def setup():
	shelltools.system("sed -i 's|opt|usr|' src/gentables")
	shelltools.system("sed -i 's|rxvt_push_locale\ (\"\")|rxvt_push_locale (\"C\")|' src/rxvtperl.xs")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	shelltools.export("TERMINFO", "%s/usr/share/terminfo" % get.installDIR())
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/usr/lib/urxvt/perl", "tabbedalt")
