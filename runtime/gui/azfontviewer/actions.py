#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, mesontools, pisitools, get

def setup():
	autotools.rawConfigure("--prefix=/usr CFLAGS='%s'" % get.CFLAGS())

def build():
	mesontools.build()

def install():
	mesontools.install()

	pisitools.removeDir("/usr/share/doc")
