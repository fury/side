#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, shelltools, get

def setup():
	mesontools.configure("--prefix=/usr --buildtype=release")

def build():
	mesontools.build()

def install():
	mesontools.install()

	datapath = "%s/usr/share/copyright/viewnior" % get.installDIR()
	for t in shelltools.ls(datapath):
		shelltools.chmod("%s/%s" % (datapath, t), mode = 0644)
