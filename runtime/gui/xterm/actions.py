#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

a = "sed -i '/v0/{n;s/new:/new:kb=^?:/}' termcap && printf '\tkbs=\\177,\n' >> terminfo"

u = ''.join([
    ' --prefix=/usr',
    ' --with-utempter',
    ' --with-app-defaults=/usr/share/X11/app-defaults'
    ])

def setup():
	shelltools.system(a)
	shelltools.export("TERMINFO", "/usr/share/terminfo")
	autotools.configure(u)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for i in ["xterm", "uxterm"]:
		pisitools.insinto("/usr/share/applications", "%s.desktop" % i)
