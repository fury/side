#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools

def install():
	shelltools.cd("colorschemes")
	for scheme in shelltools.ls("."):
		shelltools.chmod(scheme, mode = 0644)
		pisitools.insinto("/usr/share/geany/colorschemes", scheme)
