#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	pisitools.dosed("Makefile.in", "po\ doc\ data", "po data")
	autotools.configure("--prefix=/usr --enable-gtk2 --disable-vte")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for t in ["pspec.xml", "actions.py"]:
		pisitools.insinto("/usr/share/geany/templates/files", t)
