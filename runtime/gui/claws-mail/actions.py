#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --enable-generic-umpc',
    ' --disable-python-plugin',
    ' --disable-fancy-plugin',
    ' --disable-jpilot',
    ' --disable-enchant',
    ' --disable-manual',
    ' --disable-valgrind',
    ' --disable-dillo-plugin',
    ' --disable-tnef_parse-plugin',
    ' --disable-static '
    ])
def setup():
	pisitools.dosed("Makefile.in", "RELEASE_NOTES", deleteLine = True)
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
