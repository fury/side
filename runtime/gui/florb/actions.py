#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, get

def setup():
	import urllib
	patchfile = open("patchlist", "r")
	f = patchfile.read()
	for l in f.splitlines():
		patch = l.rsplit("/",1)[-1]
		urllib.urlretrieve(l, patch)
		shelltools.system("patch -p1 -i %s" % patch)

def build():
	shelltools.cd("src")
	for t in ["", "i18nupdate", "i18ncompile"]:
		shelltools.system("omake %s" % t)

def install():
	destdir = get.installDIR()
	shelltools.cd("src")
	shelltools.system("omake PREFIX=%s/usr install" % destdir)

	for i in ["florb.xpm", "res/florb.png", "res/florb.svg"]:
		pisitools.insinto("/usr/share/pixmaps", i)

	for l in ["de_DE", "sv_SE"]:
		p = "%s/usr/share/locale/%s/LC_MESSAGES/florb.mo" % (destdir, l)
		shelltools.chmod(p, mode = 0644)

	pisitools.removeDir("/usr/share/res")
