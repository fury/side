#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

def setup():
	autotools.configure("--prefix=/usr --disable-print")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	datapath = "%s/usr/share/copyright/leafpad" % get.installDIR()
	for t in shelltools.ls(datapath):
		shelltools.chmod("%s/%s" % (datapath, t), mode = 0644)
