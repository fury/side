#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	pisitools.dosed("data/lxinput.desktop.in", "NotShowIn", deleteLine = True)
	autotools.autoreconf("-fiv")
	autotools.configure("--prefix=/usr --disable-gtk3")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
