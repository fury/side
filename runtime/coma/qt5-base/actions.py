#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' -prefix /usr',
    ' -sysconfdir /etc/xdg',
    ' -libdir /usr/lib',
    ' -bindir /usr/lib/qt5/bin',
    ' -archdatadir /usr/lib/qt5',
    ' -plugindir /usr/lib/qt5/plugins',
    ' -importdir /usr/lib/qt5/imports',
    ' -headerdir /usr/include/qt5',
    ' -datadir /usr/share/qt5',
    ' -translationdir /usr/share/qt5/translations',
    ' -examplesdir /usr/share/doc/qt5/examples',
    ' -confirm-license',
    ' -opensource',
    ' -dbus-linked',
    ' -openssl-linked',
    ' -system-harfbuzz',
    ' -system-sqlite',
    ' -nomake tests',
    ' -nomake examples',
    ' -no-rpath',
    ' -syslog '
    ])

def setup():
	a = "^enum\ {\ defaultSystemFontSize\ =\ 9\ };"
	b = "enum { defaultSystemFontSize = 10 };"
	pisitools.dosed("src/platformsupport/themes/genericunix/qgenericunixthemes.cpp", a, b)
	autotools.rawConfigure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("INSTALL_ROOT=%s" % get.installDIR())

	for t in shelltools.ls("%s/usr/lib/qt5/bin/" % get.installDIR()):
		if t.startswith(tuple({"qmake", "qdbus"})):
			pisitools.dosym("/usr/lib/qt5/bin/%s" % t, "/usr/bin/%s-qt5" % t)
		else:
			pisitools.dosym("/usr/lib/qt5/bin/%s" % t, "/usr/bin/%s" % t)

	for t in shelltools.ls("%s/usr/lib" % get.installDIR()):
		if t.endswith("prl"):
			pisitools.dosed("%s/usr/lib/%s" % (get.installDIR(), t), "QMAKE_PRL_BUILD_DIR", deleteLine = True)

	pisitools.removeDir("/usr/share/qt5")
