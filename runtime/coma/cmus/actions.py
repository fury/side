#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

i = ''.join([
    ' prefix=/usr',
    ' CONFIG_AAUDIO=n',
    ' CONFIG_ARTS=n',
    ' CONFIG_COREAUDIO=n',
    ' CONFIG_DISCID=n',
    ' CONFIG_ROAR=n',
    ' CONFIG_SNDIO=n',
    ' CONFIG_SUN=n',
    ' CONFIG_TREMOR=n',
    ' CONFIG_VTX=n '
    ])

def setup():
	autotools.rawConfigure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
