#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-png',
    ' --enable-xft',
    ' --enable-jpeg',
    ' --enable-utf8',
    ' --disable-printing',
    ' --disable-static '
    ])

def setup():
	# disallow build demos.
	shelltools.system("sed -i '/doc/d' Makefile.am")
	shelltools.system("sed -i 's/demos/doc/' Makefile.am")
	# reconfigure upstream.
	shelltools.system("touch NEWS")
	autotools.autoreconf("-fiv")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
