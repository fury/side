#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --enable-release',
    ' --disable-jp2',
    ' --disable-cups',
    ' --disable-native',
    ' --disable-static '
    ])

def setup():
	pisitools.dosed("Makefile.am", "\ doc[a-z, ].*", " tests glviewer")
	pisitools.dosed("configure.ac", "-fno-stack-protector", "-fstack-protector-strong")
	autotools.autoreconf("-fiv")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
