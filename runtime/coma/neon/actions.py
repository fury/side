#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-shared',
    ' --with-ssl',
    ' --with-ca-bundle=/etc/pki/tls/certs/ca-bundle.crt',
    ' --disable-nls',
    ' --disable-static '
    ])

def setup():
	pisitools.dosed("Makefile.in", "install-html$", "")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
