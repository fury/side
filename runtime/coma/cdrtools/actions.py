#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' INS_BASE=/usr',
    ' DEFINSUSR=root',
    ' DEFINSGRP=root',
    ' MANSUFF_LIB=3cdr '
    ])

shelltools.export("GMAKE_NOWARN", "true")

def build():
	autotools.make("-j1 INS_BASE=/usr DEFINSUSR=root DEFINSGRP=root")

def install():
	autotools.rawInstall("DESTDIR=%s %s" % (get.installDIR(), i))

	pisitools.removeDir("/usr/share/doc")
	for l in ["GPL-2.0.txt", "LGPL-2.1.txt"]:
		pisitools.insinto("/usr/share/copyright/cdrtools", l)
