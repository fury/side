#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

themes = ["clearlooks", "crux", "glide", "hc", "industrial", "mist", "redmond", "thinice"]

def setup():
	shelltools.system("sh autogen.sh")
	autotools.configure("--prefix=/usr")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	a = "AUTHORS"
	for b in (tuple(themes)):
		shelltools.cd("engines/%s" % b)
		shelltools.chmod(a, mode = 0644)
		pisitools.insinto("/usr/share/copyright/gtk-engines", a, shelltools.baseName(a).replace(a, "%s-%s" % (a, b)))
		shelltools.cd("../..")
