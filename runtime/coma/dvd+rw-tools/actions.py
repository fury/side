#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

def setup():
	shelltools.system("sed -i '/stat.h/a #include <sys/sysmacros.h>' growisofs.c")
	shelltools.system("sed -i '/stdlib/a #include <limits.h>' transport.hxx")

def build():
	autotools.make("all rpl8")

def install():
	autotools.rawInstall("DESTDIR=%s prefix=%s/usr" % (get.installDIR(), get.installDIR()))
