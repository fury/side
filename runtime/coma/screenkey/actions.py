#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, pisitools

def build():
	pisitools.dosed("setup.py", "doc", deleteLine = True)
	pythonmodules.compile(pyVer = "3")

def install():
	pythonmodules.install(pyVer = "3")
