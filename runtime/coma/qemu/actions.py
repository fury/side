#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --localstatedir=/var',
    ' --target-list=x86_64-softmmu',
    ' --audio-drv-list=alsa',
    ' --enable-slirp',
    ' --enable-docs',
    ' --disable-pa '
    ])

NoStrip = ["/usr/share/qemu"]

def setup():
	pisitools.dosed("qga/meson.build", "install_emptydir", "# install_emptydir")
	pisitools.dosed("docs/meson.build", "install_subdir", deleteLine = True)
	autotools.configure(i)

def build():
	autotools.make("-C build")

def install():
	autotools.rawInstall("-C build DESTDIR=%s" % get.installDIR())
	pisitools.dosym("qemu-system-x86_64", "/usr/bin/qemu")
	shelltools.chmod("%s/usr/libexec/qemu-bridge-helper" % get.installDIR(), mode = 04750)
