#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools, pisitools

def setup():
	cmaketools.configure("-DBUILD_SHARED_LIBS=ON -Bbuild -G Ninja -L")

def build():
	mesontools.build()

def install():
	mesontools.install()

	pisitools.dosym("wodim", "/usr/bin/cdrecord")
	pisitools.dosym("readom", "/usr/bin/readcd")
	pisitools.dosym("genisoimage", "/usr/bin/mkisofs")  
	pisitools.dosym("genisoimage", "/usr/bin/mkhybrid")	
	pisitools.dosym("icedax", "/usr/bin/cdda2wav")

	pisitools.dosym("wodim.1", "/usr/share/man/man1/cdrecord.1")
	pisitools.dosym("readom.1", "/usr/share/man/man1/readcd.1")
	pisitools.dosym("genisoimage.1", "/usr/share/man/man1/mkisofs.1")
	pisitools.dosym("genisoimage.1", "/usr/share/man/man1/mkhybrid.1")
	pisitools.dosym("icedax.1", "/usr/share/man/man1/cdda2wav.1")
