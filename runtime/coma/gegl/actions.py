#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Ddocs=false',
    ' -Dvapigen=disabled',
    ' -Dlibav=disabled',
    ' -Dlibspiro=disabled',
    ' -D{libv4l,libv4l2}=disabled',
    ' -Dlua=disabled',
    ' -Dmrg=disabled',
    ' -Dmaxflow=disabled',
    ' -Dopenexr=disabled',
    ' -Dpoppler=disabled',
    ' -Dsdl{1,2}=disabled',
    ' -Dumfpack=disabled '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
