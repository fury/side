#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --disable-mtab',
    ' --disable-rpath',
    ' UDEV_RULES_PATH=/usr/lib/udev/rules.d',
    ' MOUNT_FUSE_PATH=/usr/sbin',
    ' --disable-static '
    ])

def setup():
	autotools.autoreconf("-vif")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for d in ["/dev", "/etc"]:
		pisitools.removeDir(d)
