#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

WorkDir = "openldap-LMDB_%s/libraries/liblmdb" % get.srcVERSION()

def setup():
	pisitools.dosed("lmdb.pc", "@@VERSION@@", get.srcVERSION())

def build():
	autotools.make("prefix=/usr")

def install():
	autotools.rawInstall("DESTDIR=%s prefix=/usr" % get.installDIR())

	pisitools.insinto("/usr/lib/pkgconfig/", "lmdb.pc")
