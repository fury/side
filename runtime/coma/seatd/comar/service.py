#!/usr/bin/python2
# -*- coding: utf-8 -*-

from comar.service import *
import os

serviceType="server"
serviceDesc=_({"en": "seat daemon"})

DAEMON="/usr/bin/seatd"
PIDFILE="/run/seatd.pid"
serviceDefault="off"

@synchronized
def start():
  startService(command=DAEMON, pidfile=PIDFILE, args="-g video", makepid=True, detach=True, donotify=True)

@synchronized
def stop():
  stopService(pidfile=PIDFILE, donotify=True)

  try:
    os.unlink(PIDFILE)
  except OSError:
    pass

def status():
  return isServiceRunning(PIDFILE)
