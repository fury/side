#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools, pythonmodules

i = ''.join([
    ' -DPYBIND11_FINDPYTHON=ON',
    ' -D{USE_PYTHON_INCLUDE_DIR,PYBIND11_TEST}=OFF',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()
	pythonmodules.compile(pyVer = "3")

def install():
	mesontools.install()
	pythonmodules.install(pyVer = "3")
