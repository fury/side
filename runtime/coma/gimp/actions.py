#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --without-libmng',
    ' --without-libheif',
    ' --without-openexr',
    ' --without-jpegxl',
    ' --without-webkit',
    ' --without-wmf',
    ' --without-alsa',
    ' --disable-check-update',
    ' --disable-static '
    ])

def setup():
	# do not install html documentation
	shelltools.system("sed -i '/[ ]*devel-docs/d' Makefile.in")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
