#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

y = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dinstall_tools=false',
    ' -Dgtk_doc=false',
    ' -Ddeprecation_flags=true '
    ])

def setup():
	mesontools.configure(y)

def build():
	mesontools.build()

def install():
	mesontools.install()
