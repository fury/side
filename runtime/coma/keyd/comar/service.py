#!/usr/bin/python2
# -*- coding: utf-8 -*-

from comar.service import *
import os

serviceType = "local"
serviceDesc = _({"en": "Key remapping daemon"})
serviceDefault = "on"

PIDFILE="/run/keyd.pid"

@synchronized
def start():
    startService(command="/usr/bin/keyd", pidfile=PIDFILE, makepid=True, donotify=True, detach=True)

@synchronized
def stop():
    stopService(pidfile=PIDFILE, donotify=True)

    try:
        os.unlink(PIDFILE)
    except:
        pass

def reload():
    stop()
    start()

def status():
    return isServiceRunning(pidfile=PIDFILE)
