#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	pisitools.dosed("Makefile", "\ \|\ gzip", "")
	pisitools.dosed("Makefile", "1.gz", "1")
	pisitools.dosed("Makefile", "-groupadd", "# -groupadd")
	pisitools.dosed("Makefile", "install\ -m644\ docs", deleteLine = True)

def build():
	autotools.make()
	autotools.make("man")

def install():
	autotools.rawInstall("DESTDIR=%s PREFIX=/usr" % get.installDIR())
