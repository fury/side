#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-{udisks,exif}',
    ' --with-gtk=3',
    ' --disable-old-actions',
    ' --disable-static '
    ])

def setup():
	shelltools.system("./autogen.sh")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	# drop extra libs
	for l in shelltools.ls("%s/usr/lib" % get.installDIR()):
		if l.startswith("libfm-extra"):
			pisitools.remove("/usr/lib/%s" % l)

	for l_ in ["fm-extra.h", "fm-version.h", "fm-xml-file.h"]:
		pisitools.remove("/usr/include/libfm-1.0/%s" % l_)

	pisitools.remove("/usr/lib/pkgconfig/libfm-extra.pc")
