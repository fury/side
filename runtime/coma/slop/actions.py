#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools, pisitools

i = ''.join([
    ' -DCMAKE_INSTAL_PREFIX=/usr',
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	pisitools.dosed("CMakeLists.txt", "CXX_STANDARD\ 11", "CXX_STANDARD 17")
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
