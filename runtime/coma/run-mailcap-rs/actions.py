#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

shelltools.export("RUSTUP_TOOLCHAIN", "stable")

# use --ignore-sandbox
def setup():
	shelltools.system("cargo fetch --target 'x86_64-unknown-linux-gnu'")

def build():
	shelltools.system("cargo build --frozen --release --all-features")

def install():
	autotools.rawInstall("PREFIX=%s/usr COMPAT_LINKS=1" % get.installDIR())
