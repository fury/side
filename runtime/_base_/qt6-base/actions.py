#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools, pisitools

i = ''.join([
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DINSTALL_LIBEXECDIR=libexec',
    ' -DINSTALL_ARCHDATADIR=lib/qt6',
    ' -DINSTALL_DATADIR=share',
    ' -DINSTALL_MKSPECSDIR=lib/qt6/mkspecs',
    ' -DINSTALL_PLUGINSDIR=lib/qt6/plugins',
    ' -DINSTALL_DESCRIPTIONSDIR=lib/qt6/modules',
    ' -DINSTALL_QMLDIR=lib/qt6/qml',
    ' -DFEATURE_system_xcb_xinput=ON',
    ' -DFEATURE_no_direct_extern_access=ON',
    ' -DFEATURE_system_sqlite=ON',
    ' -DFEATURE_openssl_linked=ON',
    ' -Wno-dev -Bbuild -G Ninja -L '
    ])

def setup():
	a = "^enum\ {\ defaultSystemFontSize\ =\ 9\ };"
	b = "enum { defaultSystemFontSize = 10 };"
	pisitools.dosed("src/gui/platform/unix/qgenericunixthemes.cpp", a, b)
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	pisitools.removeDir("/usr/share")
