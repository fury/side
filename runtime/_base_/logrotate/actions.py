#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-acl',
    ' --with-compress-command=/usr/bin/gzip',
    ' --with-uncompress-command=/usr/bin/gunzip '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("check")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/etc/cron.daily", "examples/logrotate.cron", "logrotate.sh")
