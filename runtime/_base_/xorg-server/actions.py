#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' --localstatedir=/var',
    ' -Dglamor=true',
    ' -Dlibunwind=true',
    ' -Dxephyr=true',
    ' -Dxcsecurity=true',
    ' -Dxkb_output_dir=/var/lib/xkb '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def check():
#	mesontools.build("test")
	pass

def install():
	mesontools.install()

	pisitools.dodir("/etc/X11/xorg.conf.d")
