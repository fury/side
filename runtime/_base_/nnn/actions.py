#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, shelltools, get

i = "PREFIX=/usr"

def build():
	autotools.make(i)

def install():
	autotools.rawInstall("DESTDIR=%s %s" % (get.installDIR(), i))

	pisitools.insinto("/usr/share/bash-completion/completions", "misc/auto-completion/bash/nnn-completion.bash", "nnn")
	pisitools.insinto("/usr/share/applications", "misc/desktop/nnn.desktop")
	for a in shelltools.ls("misc/logo"):
		pisitools.insinto("/usr/share/pixmaps", "misc/logo/%s" % a, shelltools.baseName(a).replace("logo", "nnn"))

	pisitools.dosym("nnn-64x64.png", "/usr/share/pixmaps/nnn.png")
