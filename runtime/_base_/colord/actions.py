#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, shelltools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dvapi=true',
    ' -Ddaemon_user=colord',
    ' -Dlibcolordcompat=true',
    ' -Dargyllcms_sensor=false',
    ' -D{docs,systemd}=false '
    ])

def setup():
	shelltools.chmod("data/colormgr", mode = 0644)
	pisitools.dosed("meson.build", "^subdir\('po'\)", deleteLine = True)
	pisitools.dosed("man/meson.build", "xsl-ns", "xsl")
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
