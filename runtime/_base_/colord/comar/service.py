from comar.service import *
import os

serviceType = "server"
serviceDesc = _({"en": "Color Management Daemon"})
serviceDefault = "off"

pidFile = "/run/colord.pid"

@synchronized
def start():
  startService(command="/usr/libexec/colord",
               pidfile=pidFile,
               makepid=True,
               detach=True,
               donotify=True)

@synchronized
def stop():
  stopService(pidfile=pidFile,
              donotify=True)

  os.unlink(pidFile)

def status():
  return isServiceRunning(pidFile)
