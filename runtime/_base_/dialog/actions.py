#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

def setup():
	autotools.configure("--prefix=/usr -with-ncursesw --enable-pc-files --with-libtool")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	l = "%s/usr/lib/libdialog.so.15.0.0" % get.installDIR()
	shelltools.chmod(l, mode = 0755)
