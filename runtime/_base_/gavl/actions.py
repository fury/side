#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-cpuflags=none',
    ' --disable-cpu-clip',
    ' --disable-static',
    ' --without-doxygen '
    ])

def setup():
	shelltools.system("sh autogen.sh")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	shelltools.unlink("COPYING")
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
