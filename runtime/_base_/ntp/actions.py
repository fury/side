#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --bindir=/usr/sbin',
    ' --enable-linuxcaps',
    ' --with-lineeditlibs=readline '
    ])

def setup():
	for a in ["configure", "sntp/configure"]:
		shelltools.system("sed -e 's;pthread_detach(NULL);pthread_detach(0);' -i %s" % a)
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dodir("/var/lib/ntp")

	for dirs in ["/usr/libexec", "/usr/share/man/man8", "/usr/share/doc"]:
		pisitools.removeDir(dirs)
