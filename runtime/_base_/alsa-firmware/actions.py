#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	autotools.configure("--enable-buildfw --with-hotplug-dir=/usr/lib/firmware")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	# Fix linux-firmware conflict.
	pisitools.remove("/usr/lib/firmware/ctefx.bin")
