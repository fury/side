#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

install_options = ''.join([
' TO_BIN="lua5.2 luac5.2"',
' TO_LIB="liblua5.2.so liblua5.2.so.5.2 liblua5.2.so.5.2.4"',
' INSTALL_DATA="cp -d"',
' INSTALL_TOP=%s/usr' % get.installDIR(),
' INSTALL_MAN=%s/usr/share/man/man1 ' % get.installDIR()
])

def setup():
	shelltools.system("sed -i '/#define LUA_ROOT/s:/usr/local/:/usr/:' src/luaconf.h")
	shelltools.system("sed -r -e '/^LUA_(SO|A|T)=/ s/lua/lua5.2/' -e '/^LUAC_T=/ s/luac/luac5.2/' -i src/Makefile")

def build():
	autotools.make("MYCFLAGS+='-fPIC' linux")

def install():
	autotools.rawInstall("%s install" % install_options)
	pisitools.insinto("/usr/lib/pkgconfig", "lua52.pc")

	pisitools.dosym("liblua5.2.so", "/usr/lib/liblua.so.5.2")
	pisitools.dosym("liblua5.2.so", "/usr/lib/liblua.so.5.2.4")

	pisitools.rename("/usr/share/man/man1/lua.1", "lua52.1")
	pisitools.rename("/usr/share/man/man1/luac.1", "luac52.1")

	pisitools.removeDir("/usr/share/lua")
	pisitools.removeDir("/usr/lib/lua")
