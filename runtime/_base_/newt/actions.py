#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

dosed = ''.join([
'''sed -e '/install -m 644 $(LIBNEWT)/ s/^/#/' ''',
'''-e '/$(LIBNEWT):/,/rv/ s/^/#/' ''',
'''-e 's/$(LIBNEWT)/$(LIBNEWTSH)/g' -i Makefile.in '''
])

i = ''.join([
    ' --prefix=/usr',
    ' --with-gpm-support',
    ' --with-python=python3.12',
    ' --without-tcl '
    ])

def setup():
	shelltools.system(dosed)
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
