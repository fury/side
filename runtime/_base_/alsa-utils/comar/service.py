from comar.service import *
import os

serviceType = "local"
serviceDesc = _({"en": "Store/Restore Mixer Levels"})
serviceDefault = "on"

BIN="/usr/sbin/alsactl"
PIDFILE="/run/alsactl.pid"
STATEFILE="/var/lib/alsa/asound.state"

@synchronized
def start():
    if not os.path.exists(STATEFILE):
        try:
            run("%s -L store" % BIN)
        except:
            pass
    startService(command=BIN, args="daemon -b", pidfile=PIDFILE, detach=True, donotify=True)
    run("%s --no-ucm restore" % BIN)

@synchronized
def stop():
    run("%s --no-ucm store" % BIN)
    stopService(pidfile=PIDFILE, donotify=True)

def status():
    return isServiceRunning(pidfile=PIDFILE)
