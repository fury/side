#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dgtk_doc=false '
    ])

def setup():
	pisitools.dosed("meson.build", "\'po\'", deleteLine = True)
	mesontools.configure(i, build_dir = "_build")

def build():
	mesontools.build("-C _build")

def install():
	mesontools.install("-C _build")
