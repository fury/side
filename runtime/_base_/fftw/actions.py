#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-threads',
    ' --enable-shared',
    ' --disable-static '
    ])

def setup():
	shelltools.copytree("../fftw-%s" % get.srcVERSION(), "../single")
	shelltools.copytree("../fftw-%s" % get.srcVERSION(), "../double")

	autotools.configure("%s --enable-long-double" % i)

	shelltools.cd("../single")
	autotools.configure("%s --enable-sse2 --enable-avx --enable-avx2 --enable-float" % i)

	shelltools.cd("../double")
	autotools.configure("%s --enable-sse2 --enable-avx --enable-avx2" % i)

def build():
	for t in [".", "single", "double"]:
		shelltools.cd(t)
		autotools.make()
		shelltools.cd("..")

def install():
	for t in [".", "single", "double"]:
		shelltools.cd(t)
		autotools.rawInstall("DESTDIR=%s" % get.installDIR())
		shelltools.cd("..")
