#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dfuse=true',
    ' -Dgphoto2=true',
    ' -Dman=true',
    ' -Dmtp=true',
    ' -Dlogind=true',
    ' -Dafc=false',
    ' -Dnfs=false',
    ' -Dsmb=false',
    ' -Ddnssd=false',
    ' -Dgoa=false',
    ' -Dgoogle=false',
    ' -Donedrive=false',
    ' -Dtmpfilesdir=no',
    ' -Dsystemduserunitdir=no '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
