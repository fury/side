#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, shelltools, docopyright

i = "".join([
    " FC-LANG= ",
    " BLOCKS='/usr/share/unicode/ucd/Blocks.txt'",
    " UNICODEDATA='/usr/share/unicode/ucd/UnicodeData.txt'"
    ])

def build():
	autotools.make(i)

def install():
	for t in shelltools.ls("build"):
		if t.endswith("ttf"):
			pisitools.insinto("/usr/share/fonts/DejaVu", "build/%s" % t)

	pisitools.dosym("/usr/share/fonts/DejaVu" , "/etc/X11/fontpath.d/DejaVu")

	for t in shelltools.ls("fontconfig"):
		pisitools.insinto("/etc/fonts/conf.avail", "fontconfig/%s" % t)
		pisitools.dosym("/etc/fonts/conf.avail/%s" % t, "/etc/fonts/conf.d/%s" % t)

	docopyright.installCopyright()
