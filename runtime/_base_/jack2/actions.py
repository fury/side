#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, shelltools, get, docopyright
import os

del(os.environ["JOBS"])

i = ''.join([
    ' --prefix=/usr',
    ' --libdir=/usr/lib',
    ' --alsa',
    ' --dbus '
    ])

def setup():
	shelltools.system("patch -p1 -i ../waf.patch")
	pythonmodules.run("waf configure %s" % i, pyVer = "3")

def build():
	pythonmodules.run("waf build -v", pyVer = "3")

def install():
	pythonmodules.run("waf --destdir=%s install" % get.installDIR(), pyVer = "3")

	docopyright.installCopyright()
