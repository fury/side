#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools

WorkDir = "."

def setup():
	shelltools.makedirs("CPP/7zip/Bundles/Alone2/b/g")
	shelltools.system("find . -type f -exec chmod 0644 {} \;")

def build():
	shelltools.cd("CPP/7zip/Bundles/Alone2")
	autotools.make("DISABLE_RAR=1 -f ../../cmpl_gcc.mak")

def install():
	pisitools.dobin("CPP/7zip/Bundles/Alone2/b/g/7zz")
	pisitools.dosym("7zz", "/usr/bin/7z")

	for l in ["copying.txt", "License.txt", "unRarLicense.txt"]:
		pisitools.insinto("/usr/share/copyright/7z", "DOC/%s" % l)
