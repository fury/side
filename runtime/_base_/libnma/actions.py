#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dlibnma_gtk4=false',
    ' -Dgtk_doc=false '
    ])

def setup():
	pisitools.dosed("meson.build", "org.gnome.nm-applet.gschema.xml", deleteLine = True)
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
