#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' DISABLE_{SOUND,DESKTOP}_NOTIFY=0',
    ' ENABLE_TOX_EXPERIMENTAL=1',
    ' DISABLE_{AV,VI}=0',
    ' DISABLE_X11=0',
    ' DISABLE_GAMES=1 '
    ])

def setup():
	pisitools.dosed("misc/toxic.desktop", "AudioVideo;", "")
	pisitools.dosed("cfg/targets/install.mk", "gzip", deleteLine = True)

def build():
	autotools.make(i)

def install():
	autotools.rawInstall("DESTDIR=%s PREFIX=/usr" % get.installDIR())
