#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DCAMKE_INSTALL_PREFIX=/usr',
    ' -DENABLE_UNSTABLE_API_ABI_HEADERS=ON',
    ' -DENABLE_QT5=OFF',
    ' -DENABLE_QT6=OFF',
    ' -DENABLE_LIBCURL=OFF',
    ' -DENABLE_ZLIB_UNCOMPRESS=ON',
    ' -Bbuild -G Ninja -L',
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
