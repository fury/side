#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pisitools, docopyright

def install():
	pisitools.dobin("inxi")
	pisitools.doman("inxi.1")
	docopyright.installCopyright()
