#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-man-doc',
    ' --with-json',
    ' --with-xtables',
    ' --with-mini-gmp',
    ' --with-cli=readline',
    ' --disable-static'
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()
	pythonmodules.makewhl("py")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	params =''.join([
	' -m installer',
	' --no-compile-bytecode',
	' --destdir=%s' % get.installDIR(),
	' py/dist/nftables-0.1-py3-none-any.whl '
	])

	pythonmodules.run(params, pyVer = "3")
