#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

def setup():
	shelltools.copy("pic/gifgrid.gif", "doc/giflib-logo.gif")

def build():
	autotools.make()

def install():
	autotools.rawInstall("PREFIX=/usr DESTDIR=%s" % get.installDIR())

	pisitools.remove("/usr/lib/libgif.a")
