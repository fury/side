#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --localstatedir=/var',
    ' --enable-{gettext,static}=no '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make("DEBUG=-DNDEBUG INSTALL_USER=root INSTALL_GROUP=root")

def install():
	for a in ["", "install-dev"]:
		autotools.rawInstall("DIST_ROOT=%s %s" % (get.installDIR(), a))

	pisitools.removeDir("/usr/share/doc")
	for l in ["doc/CREDITS", "LICENSES/GPL-2.0", "LICENSES/LGPL-2.1"]:
		pisitools.insinto("/usr/share/copyright/xfsprogs", l)
