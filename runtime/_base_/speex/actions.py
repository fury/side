#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

WorkDir = "."

a = "speex-Speex-%s" % get.srcVERSION()
b = "speexdsp-SpeexDSP-%s" % get.srcVERSION()

def setup():
	for i in [a, b]:
		shelltools.cd(i)
		shelltools.system("sed -i 's|\ doc||' Makefile.am")
		shelltools.system("NOCONFIGURE=1 ./autogen.sh")
		autotools.configure("--prefix=/usr --disable-static")
		shelltools.cd("..")

def build():
	for i in [a, b]:
		shelltools.cd(i)
		autotools.make()
		shelltools.cd("..")

def install():
	for i in [a, b]:
		shelltools.cd(i)
		autotools.rawInstall("DESTDIR=%s" % get.installDIR())
		shelltools.cd("..")
