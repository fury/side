#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

WorkDir = "."
SkipList = (".", "filelist", "patches", "pisiBuildState", "tmp")

i = ''.join([
    ' --prefix=/usr',
    ' --disable-dependency-tracking',
    ' --disable-selective-werror'
    ])

def setup():
	for package in shelltools.ls("."):
		if package.startswith(SkipList):
			continue

		print "\nConfiguring %s..." % package
		print "-" * (len(package) + 15)
		shelltools.cd(package)

		# separate copyright
		b = package.rsplit("-",1)[0]
		for a in shelltools.ls("."):
			if a.startswith(("AUTHORS", "COPYING")):
				shelltools.move(a, "%s-%s" % (a, b))

		if not shelltools.isFile("configure"):
			autotools.autoreconf("-vif")

		autotools.configure(i)
		shelltools.cd("..")

def build():
	for package in shelltools.ls("."):
		if package.startswith(SkipList):
			continue

		shelltools.cd(package)
		autotools.make()
		shelltools.cd("..")

def install():
	for package in shelltools.ls("."):
		if package.startswith(SkipList):
			continue

		shelltools.cd(package)
		autotools.rawInstall("DESTDIR=%s" % get.installDIR())
		shelltools.cd("..")
