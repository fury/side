#!/usr/bin/python2
# -*- coding: utf-8 -*-

from comar.service import *
import os

serviceType="server"
serviceDesc=_({"en": "sddm Server"})
serviceDefault="on"

DAEMON="/usr/bin/sddm"
PIDFILE="/run/sddm.pid"

@synchronized
def start():
  startService(command=DAEMON, pidfile=PIDFILE, makepid=True, detach=True, donotify=True)

@synchronized
def stop():
  stopService(pidfile=PIDFILE, donotify=True)

  try:
    os.unlink(PIDFILE)
  except OSError:
    pass

def status():
  return isServiceRunning(PIDFILE)
