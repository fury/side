#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -D{NO_SYSTEMD,USE_ELOGIND}=ON',
    ' -DBUILD_{WITH_QT6,MAN_PAGES}=ON',
    ' -D{ENABLE_JOURNALD,INSTALL_PAM_CONFIGURATION}=OFF',
    ' -DRUNTIME_DIR=/run/sddm',
#    ' -DSDDM_INITIAL_VT=7',
    ' -DDBUS_CONFIG_DIR=/usr/share/dbus-1/system.d',
    ' -DDBUS_CONFIG_FILENAME=sddm_org.freedesktop.DisplayManager.conf',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
