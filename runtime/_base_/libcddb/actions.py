#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

def setup():
	shelltools.system("sed -e '/DEFAULT_SERVER/s/freedb.org/gnudb.gnudb.org/' -e '/DEFAULT_PORT/s/888/&0/' -i include/cddb/cddb_ni.h")
	shelltools.system("sed '/^Genre:/s/Trip-Hop/Electronic/' -i tests/testdata/920ef00b.txt")
	shelltools.system("sed '/DISCID/i# Revision: 42' -i tests/testcache/misc/12340000")
	autotools.configure("--prefix=/usr --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
