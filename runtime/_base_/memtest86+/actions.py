#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools

WorkDir = "src/build64"

def setup():
	pass

def build():
	autotools.make()

def install():
	for a in ["bin", "efi"]:
		pisitools.dobin("memtest.%s" % a, "/boot")
