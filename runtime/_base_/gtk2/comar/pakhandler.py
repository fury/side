#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, piksemel, fnmatch

def updateData(filepath):
    parse = piksemel.parse(filepath)

    iconFound = False
    immoduleFound = False

    for icon in parse.tags("File"):
        path = icon.getTagData("Path")
        if path.startswith("usr/share/icons/hicolor") and not iconFound:
            try:
                os.system("/usr/bin/gtk-update-icon-cache -f /usr/share/icons/hicolor")
            except:
                pass
            iconFound = True
            if immoduleFound:
                return

        if fnmatch.fnmatch(path, "usr/lib/gtk-2.0/*immodules/*.so") and not immoduleFound:
            os.system("/usr/bin/gtk-query-immodules-2.0 --update-cache")
            immoduleFound = True
            if iconFound:
                return

def setupPackage(metapath, filepath):
    updateData(filepath)

def cleanupPackage(metapath, filepath):
    pass

def postCleanupPackage(metapath, filepath):
    updateData(filepath)
