#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, shelltools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-xinput=yes',
    ' --with-gdktarget=x11',
    ' --with-html-dir=/usr/share/doc/gtk2/html',
    ' --enable-man',
    ' --disable-cups',
    ' --disable-static '
    ])

def setup():
	shelltools.system("sed -e 's#l \(gtk-.*\).sgml#& -o \1#' -i docs/{faq,tutorial}/Makefile.in")
	shelltools.system("sed -i 's#env\ python$#python2#' gtk/gtk-builder-convert")
	autotools.autoreconf("-fiv")
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("-k check")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.remove("/usr/bin/gtk-update-icon-cache")
	pisitools.remove("/usr/share/man/man1/gtk-update-icon-cache.1")
