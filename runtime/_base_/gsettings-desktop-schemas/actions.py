#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, shelltools

def setup():
	shelltools.system("sed -i \"/^subdir('po')/d\" meson.build")
	shelltools.system("sed -i -r 's:\"(/system):\"/org/gnome\1:g' schemas/*.in")
	mesontools.configure()

def build():
	mesontools.build

def install():
	mesontools.install()
