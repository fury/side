#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

import os

def setup():
	autotools.configure("--prefix=/usr --disable-nls --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	UDEV_RULES="usr/lib/udev/rules.d/40-libgphoto2.rules"
	HWDB_RULES="usr/lib/udev/rules.d/20-libgphoto2.hwdb"
	CAM_LIST="usr/lib/libgphoto2/print-camera-list"
	CAM_LIBS="usr/lib/libgphoto2/%s" % get.srcVERSION()

	shelltools.export("CAMLIBS", "%s/%s" % (get.installDIR(), CAM_LIBS))
	shelltools.export("LIBDIR", "%s/usr/lib/" % get.installDIR())
	shelltools.export("LD_LIBRARY_PATH", "%s/usr/lib/" % get.installDIR())

	# Generate UDEV rule
	pisitools.dodir("/usr/lib/udev/rules.d")
	f = open(os.path.join(get.installDIR(), UDEV_RULES), "w")
	f.write(os.popen("%s/%s udev-rules version 201" % (get.installDIR(), CAM_LIST)).read())
	f.close()

	# Generate hardware database
	f = open(os.path.join(get.installDIR(), HWDB_RULES), "w")
	f.write(os.popen("%s/%s hwdb" % (get.installDIR(), CAM_LIST)).read())
	f.close()

	for t in ["doc", "libgphoto2", "libgphoto2_port"]:
		pisitools.removeDir("/usr/share/%s" % t)
