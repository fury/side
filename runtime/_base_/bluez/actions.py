#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-{library,threads}',
    ' --with-udevdir=/usr/lib/udev',
    ' --disable-{cups,systemd,static} '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	pisitools.insinto("/usr/share/dbus-1/services", "obexd/src/org.bluez.obex.service")
	pisitools.dosym("/usr/libexec/bluetooth/bluetoothd", "/usr/sbin/bluetoothd")
