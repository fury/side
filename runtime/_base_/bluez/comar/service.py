#!/usr/bin/python2
# -*- coding: utf-8 -*-

from comar.service import *
import os

serviceType = "local"
serviceDesc = _({"en": "Bluetooth Service"})
serviceDefault = "on"

PIDFILE="/run/bluez.pid"
DAEMON ="/usr/libexec/bluetooth/bluetoothd"

@synchronized
def start():
  startService(command=DAEMON,
               pidfile=PIDFILE,
               makepid=True,
               detach=True,
               donotify=True)

@synchronized
def stop():
  stopService(pidfile=PIDFILE,
              donotify=True)

  try:
    os.unlink(PIDFILE)
  except:
    pass

def status():
  return isServiceRunning(pidfile=PIDFILE)
