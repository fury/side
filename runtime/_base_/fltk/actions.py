#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, shelltools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-xdbe',
    ' --enable-cairo',
    ' --enable-shared=yes '
    ])

def setup():
	pisitools.dosed("documentation/Makefile", "cat[1-6].*", deleteLine = True)
	autotools.configure(i)

def build():
	autotools.make()
	autotools.make("-C documentation html")

def install():
	autotools.rawInstall("DESTDIR=%s docdir=/usr/share/doc/fltk-1.3" % get.installDIR())
	pisitools.insinto("/usr/share/doc/fltk-1.3/", "documentation/html/search")

	for t__1 in shelltools.ls("%s/usr/lib" % get.installDIR()):
		if t__1.endswith(".a"):
			pisitools.remove("/usr/lib/%s" % t__1)
