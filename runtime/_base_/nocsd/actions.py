#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, docopyright

def build():
	autotools.make()

def install():
	pisitools.dolib_so("libgtk3-nocsd.so.0")
	pisitools.doman("gtk3-nocsd.1")
	docopyright.installCopyright()
