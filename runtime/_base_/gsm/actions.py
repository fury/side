#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

pkgdir = get.installDIR()

i = ''.join([
    ' INSTALL_ROOT=%s/usr' % pkgdir,
    ' GSM_INSTALL_INC=%s/usr/include/gsm' % pkgdir,
    ' GSM_INSTALL_MAN=%s/usr/share/man/man3' % pkgdir,
    ' TOAST_INSTALL_MAN=%s/usr/share/man/man1 ' % pkgdir
    ])

def build():
	autotools.make("CCFLAGS='-c %s -fPIC'" % get.CFLAGS())

def install():
	for destdir in ["bin", "lib", "include/gsm", "share/man/man1", "share/man/man3"]:
		pisitools.dodir("/usr/%s" % destdir)
	autotools.rawInstall(i)
