#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, pisitools, mesontools

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DCMAKE_SKIP_INSTALL_RPATH=YES',
    ' -DALLOW_IN_SOURCE_BUILD=1',
    ' -DJAS_ENABLE_DOC=NO',
    ' -Bjb -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build("-C jb")

def check():
	mesontools.build("test -C jb")

def install():
	mesontools.install("-C jb")

	pisitools.removeDir("/usr/share/doc")
