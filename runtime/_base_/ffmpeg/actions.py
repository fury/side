#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --mandir=/usr/share/man',
    ' --enable-gpl',
    ' --enable-version3',
    ' --enable-nonfree',
    ' --enable-libaom',
    ' --enable-libv4l2',
    ' --enable-libass',
    ' --enable-frei0r',
    ' --enable-nvdec',
    ' --enable-nvenc',
    ' --enable-libcdio',
    ' --enable-libfdk-aac',
    ' --enable-libfreetype',
    ' --enable-libopenjpeg',
    ' --enable-libmp3lame',
    ' --enable-libopus',
    ' --enable-openssl',
    ' --enable-libtheora',
    ' --enable-libvorbis',
    ' --enable-libvpx',
    ' --enable-libx264',
    ' --enable-libx265',
    ' --enable-shared',
    ' --disable-static',
    ' --disable-debug '
    ])

def setup():
	autotools.rawConfigure(i)

def build():
	autotools.make()
	autotools.make("tools/qt-faststart")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dobin("tools/qt-faststart")
	pisitools.removeDir("/usr/share/doc")
