#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools, pisitools

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=None',
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DGIT_ARCHETYPE=1 -Wno-dev',
    ' -B_build -G Ninja -L '
    ])

def setup():
	cmaketools.configure("%s" % i, sourceDir = "source")

def build():
	mesontools.build("-C _build")

def install():
	mesontools.install("-C _build")

	pisitools.remove("/usr/lib/libx265.a")
