#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

KeepSpecial=["libtool"]

i = ''.join([
    ' --prefix=/usr',
    ' --enable-hdri',
    ' --with-modules',
    ' --with-perl',
    ' --with-fftw',
    ' --with-rsvg',
    ' --with-gslib=yes',
    ' --with-dejavu-font-dir=/usr/share/fonts/DejaVu',
    ' --with-gs-font-dir=/usr/share/ghostscript/fonts',
    ' --disable-static '
    ])
def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.removeDir("/usr/share/doc")
