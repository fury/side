#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-dynamic',
    ' --enable-versioning=yes',
    ' --with-tls=openssl',
    ' --disable-debug',
    ' --disable-slapd',
    ' --disable-static '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make("depend")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for s in ["3", "8"]:
		pisitools.removeDir("/usr/share/man/man%s" % s)
	for l in shelltools.ls("%s/usr/share/man/man5/" % get.installDIR()):
		if not l.startswith("ldap.conf.5"):
			pisitools.remove("/usr/share/man/man5/%s" % l)
