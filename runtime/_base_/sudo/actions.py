#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --libexecdir=/usr/lib',
    ' --disable-nls',
    ' --with-secure-path',
    ' --with-pam',
    ' --with-env-editor',
    ' --with-linux-audit',
    ' --with-editor=/usr/bin/nano',
    ' --with-passprompt="[sudo] password for %p: "'
    ])

def setup():
	pisitools.dosed("docs/Makefile.in", "\$\(OTHER_DOCS", deleteLine = True)
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
