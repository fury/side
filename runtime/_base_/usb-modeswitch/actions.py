#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

i = ''.join([
    ' DESTDIR=%s' % get.installDIR(),
    ' PREFIX=%s/usr' % get.installDIR(),
    ' UDEVDIR=%s/usr/lib/udev' % get.installDIR(),
    ' RULESDIR=%s/usr/lib/udev/rules.d ' % get.installDIR()
    ])

def build():
	autotools.make()

def install():
	autotools.rawInstall(i)
	autotools.rawInstall("-C ../usb-modeswitch-data-20191128 %s" % i)
