#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

WorkDir = "."

def setup():
	for package in shelltools.ls("."):
		if not shelltools.isFile(package):
			shelltools.cd(package)
			autotools.configure("--with-fontrootdir=/usr/share/fonts/X11")
			shelltools.cd("..")

def build():
	for package in shelltools.ls("."):
		if not shelltools.isFile(package):
			shelltools.cd(package)
			autotools.make()
			shelltools.cd("..")

def install():
	for package in shelltools.ls("."):
		if not shelltools.isFile(package):
			shelltools.cd(package)
			autotools.rawInstall("DESTDIR=%s" % get.installDIR())
			shelltools.cd("..")

	for i in ["TTF", "OTF"]:
		pisitools.dosym("/usr/share/fonts/X11/%s" % i, "/usr/share/fonts/X11-%s" % i)
