#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools, shelltools, pythonmodules, pisitools

i = ''.join([
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DCMAKE_INSTALL_LIBDIR=lib',
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DINSTALL_PYTHON_MODULE=OFF'
    ' -Bbuild -G Ninja -L',
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

	for t in shelltools.ls("docs/man"):
		shelltools.system("rst2man docs/man/%s > build/%s" % (t, t.replace(".rst", "")))

def install():
	mesontools.install()

	for t in shelltools.ls("build"):
		if t.endswith("1"):
			pisitools.doman("build/%s" % t)

	shelltools.cd("build/apps")
	pythonmodules.install(pyVer = "3")
