from comar.service import *

serviceType = "local"
serviceDesc = _({"en": "ACPI Daemon"})
serviceDefault = "on"

PIDFILE = "/run/acpid.pid"

@synchronized
def start():
  startService(command="/usr/sbin/acpid", pidfile=PIDFILE, donotify=True)

@synchronized
def stop():
  stopService(pidfile=PIDFILE,
              donotify=True)

def status():
  return isServiceRunning(pidfile=PIDFILE)
