#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -D{systemd_journal,qrtr}=false',
    ' -Dsystemdsystemunitdir="no"',
#    ' -Dpolkit=strict',
#    ' -Dsystemd_journal=false',
    ' -Dsystemd_suspend_resume=true',
    ' -Ddbus_policy_dir=/usr/share/dbus-1/system.d',
#    ' -Dat_command_via_dbus=true',
    ' -Dudevdir=/usr/lib/udev '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
