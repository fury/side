#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, shelltools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dvala=enabled',
    ' -Dpixbuf{,-loader}=enabled',
    ' -Dintrospection=enabled',
    ' -Davif=disabled '
    ])

def setup():
	# Install man page only
	shelltools.system("sed -i '0,/install: true/ s@true@false@' doc/meson.build")
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
