#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --runstatedir=/run',
    ' --enable-cbcp',
    ' --enable-multilink',
    ' --with-pam=yes',
    ' --with-{srp,atm}=no',
    ' --disable-static '
    ])

def setup():
	pisitools.cflags.add("-D_GNU_SOURCE")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for b in ["pon", "poff", "plog"]:
		pisitools.dobin("scripts/%s" % b)
	pisitools.doman("scripts/pon.1")

	pisitools.insinto("/etc/ppp", "sample/options", "options.example")
	pisitools.insinto("/etc/pam.d", "pppd/ppp.pam", "ppp")

	for d in ["ip-up.d", "ip-down.d", "peers"]:
		pisitools.dodir("/etc/ppp/%s" % d)
