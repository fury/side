#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

i = ''.join([
    ' --prefix=/usr',
    ' -Ddemos=false',
    ' -Dd3d11=disabled',
    ' -Dxxhash=disabled',
    ' -Dshaderc=disabled '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
