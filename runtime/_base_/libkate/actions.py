#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	autotools.autoreconf("-fiv")
	pisitools.dosed("configure", "HAVE_PYTHON=yes", "HAVE_PYTHON=no")
	autotools.configure("--prefix=/usr --enable-fortify-source --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.removeDir("/usr/share/doc")
