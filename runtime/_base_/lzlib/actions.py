#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' CC=gcc',
    ' CFLAGS="%s --std=c99 -D_XOPEN_SOURCE=500"' % get.CFLAGS(),
    ' LDFLAGS="%s"' % get.LDFLAGS(),
    ' --disable-static '
    ])

def setup():
	shelltools.system("sed -i '/INSTALL_SO/s/644/755/' Makefile.in")
	autotools.rawConfigure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
