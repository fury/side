#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pisitools, scons, get

i = ''.join([
    ' prefix=/usr',
    ' sbindir=sbin',
    ' udevdir=/usr/lib/udev',
    ' target_python=python3',
    ' python_shebang=/usr/bin/python3',
    ' python_libdir=/usr/lib/python3.12/site-packages '
    ])

def setup():
	pisitools.dosed("SConscript", "^envs\ =\ {}", "envs = os.environ")
	pisitools.dosed("SConscript", "source=doc_files", deleteLine = True)
	pisitools.dosed("SConscript", "docinstall\ \+\=", "docinstall =")

def build():
	scons.make(i)

def install():
	scons.install("udev-install DESTDIR=%s %s" % (get.installDIR(), i))
