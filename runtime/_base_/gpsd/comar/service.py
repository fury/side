#!/usr/bin/python2
# -*- coding: utf-8 -*-

from comar.service import *
import os

serviceType = "local"
serviceDesc = _({"en": "GPS Daemon"})
serviceDefault = "on"
serviceConf = "gpsd"

pidfile="/run/gpsd.pid"
socket="/run/gpsd.sock"

@synchronized
def start():
    startService(command="/usr/sbin/gpsd",
                 args="%s %s -P %s -F %s" % (config.get("DEVICE"), config.get("OPTIONS"), pidfile, socket),
                 pidfile=pidfile,
                 detach=True,
                 donotify=True)

@synchronized
def stop():
    stopService(pidfile=pidfile, donotify=True)

    if os.path.exists(pidfile):
        try:
            os.unlink(pidfile)
        except:
            pass

def status():
    return isServiceRunning(pidfile)
