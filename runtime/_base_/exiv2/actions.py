#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DEXIV2_ENABLE_VIDEO=yes',
    ' -DEXIV2_ENABLE_WEBREADY=yes',
    ' -DEXIV2_ENABLE_CURL=yes',
    ' -DEXIV2_ENABLE_BROTLI=no',
    ' -DEXIV2_BUILD_SAMPLES=no',
    ' -DCMAKE_SKIP_INSTALL_RPATH=ON',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
