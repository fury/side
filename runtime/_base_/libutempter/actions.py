#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

a = "%s/usr/libexec/utempter/utempter" % get.installDIR()

def build():
	autotools.make("-C libutempter")

def install():
	shelltools.cd("libutempter")
	autotools.rawInstall("DESTDIR=%s libexecdir=/usr/libexec" % get.installDIR())

	shelltools.chown(a, gid = "utmp")
	shelltools.chmod(a, mode = 2755)
