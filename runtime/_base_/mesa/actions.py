#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools

a = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dglx=dri',
    ' -Dplatforms=x11,wayland',
    ' -Dvulkan-drivers=auto',
    ' -Dgallium-vdpau=enabled',
    ' -Dgallium-va=enabled',
    ' -Dgallium-xa=enabled',
    ' -Dglvnd=enabled',
    ' -Dllvm=enabled',
    ' -Dlibunwind=enabled',
    ' -Dvalgrind=disabled',
    ' -Ddri-drivers-path=/usr/lib/xorg/modules/dri',
    ' -Dgallium-drivers=auto '
    ])

def setup():
	mesontools.configure(a)

def build():
	mesontools.build()

def install():
	mesontools.install()

	pisitools.insinto("/usr/share/copyright/mesa", "docs/license.rst")
