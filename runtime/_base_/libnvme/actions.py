#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Ddocs=man',
    ' -Ddocs-build=false',
    ' -Dlibdbus=auto',
    ' -Dpython=disabled',
    ' -Dopenssl=disabled',
    ' -Djson-c=disabled '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def check():
	mesontools.build("test -C build")

def install():
	mesontools.install()
