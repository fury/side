#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

def build():
	autotools.make("EFIDIR=side EFI_LOADER=grubx64.efi")

def install():
	autotools.rawInstall("EFIDIR=side DESTDIR=%s" % get.installDIR())
