#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sbindir=/usr/sbin',
    ' --exec-prefix=/usr/sbin',
    ' --enable-gettext=no',
    ' INSTALL_USER=root',
    ' INSTALL_GROUP=root '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.removeDir("/usr/share/doc")
	pisitools.insinto("/usr/share/copyright/xfsdump", "doc/COPYING")
