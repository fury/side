#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	a = "^STARTFILE:https://lynx.invisible-island.net/"
	b = "STARTFILE:file:///usr/share/lynx/startlinks.html"
	pisitools.dosed("lynx.cfg", a, b)
	autotools.configure("--prefix=/usr --with-ssl --disable-nls")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
