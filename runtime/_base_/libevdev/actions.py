#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Ddocumentation=disabled '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def check():
	mesontools.build("test")

def install():
	mesontools.install()
