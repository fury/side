#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

a = ''.join([
    ' --prefix=/usr',
    ' --disable-file-attr',
    ' --disable-compress-manpg',
    ' sa_lib_dir=/usr/lib/sa',
    ' sa_dir=/var/log/sa',
    ' conf_dir=/etc/conf.d '
    ])

def setup():
	autotools.configure(a)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for cronf in ["daily", "hourly"]:
		shelltools.chmod("cron/sysstat.cron.%s" % cronf, mode = 0755)
		pisitools.insinto("/etc/cron.%s" % cronf, "cron/sysstat.cron.%s" % cronf, "sysstat")
	pisitools.removeDir("/usr/share/doc")
