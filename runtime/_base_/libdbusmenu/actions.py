#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-gtk=3',
    ' --without-gtk2',
    ' --enable-introspection',
    ' --disable-{dumper,vala,static} '
    ])

def setup():
	pisitools.dosed("libdbusmenu-gtk/Makefile.in", "-Werror", "")
	pisitools.dosed("libdbusmenu-glib/Makefile.in", "-Werror", "")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for r in ["gtk-doc", "doc"]:
		pisitools.removeDir("/usr/share/%s" % r)
