#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get, shelltools, pisitools

i = ''.join([
    ' --libdir=/usr/lib',
    ' --enable-libpaper',
    ' --with-systemd=',
    ' --with-tls=gnutls',
    ' --with-dbusdir=/usr/share/dbus-1',
    ' --with-rcdir=/usr/share/cups/examples/cupsinit',
    ' --with-rundir=/run/cups',
    ' --with-system-groups=lpadmin '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("-k check")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	shelltools.chmod("%s/run/cups/certs" % get.installDIR(), 0755)
