diff --git a/configure.ac b/configure.ac
index 8a1f0cc..bc599fd 100644
--- a/configure.ac
+++ b/configure.ac
@@ -24,14 +24,14 @@
 AC_PREREQ([2.60])
 AC_INIT([xf86-video-v4l],
         [0.3.0],
-        [https://bugs.freedesktop.org/enter_bug.cgi?product=xorg],
+        [https://gitlab.freedesktop.org/xorg/driver/xf86-video-v4l/issues],
         [xf86-video-v4l])
 AC_CONFIG_SRCDIR([Makefile.am])
 AC_CONFIG_HEADERS([config.h])
 AC_CONFIG_AUX_DIR(.)
 
 # Initialize Automake
-AM_INIT_AUTOMAKE([foreign dist-bzip2])
+AM_INIT_AUTOMAKE([foreign dist-xz])
 AM_MAINTAINER_MODE
 
 # Require X.Org macros 1.8 or later for MAN_SUBSTS set by XORG_MANPAGE_SECTIONS
@@ -41,8 +41,7 @@ XORG_MACROS_VERSION(1.8)
 XORG_DEFAULT_OPTIONS
 
 # Initialize libtool
-AC_DISABLE_STATIC
-AC_PROG_LIBTOOL
+LT_INIT([disable-static])
 
 AH_TOP([#include "xorg-server.h"])
 
@@ -60,7 +59,10 @@ XORG_DRIVER_CHECK_EXT(XV, videoproto)
 # Obtain compiler/linker options for the driver dependencies
 PKG_CHECK_MODULES(XORG, [xorg-server >= 1.0.99.901 xproto $REQUIRED_MODULES])
 
-# Checks for libraries.
+# Checks for library functions
+# We don't use strlcat or strlcpy, but check to quiet a -Wredundant-decls warning
+# from xorg/os.h which will otherwise redefine it.
+AC_CHECK_FUNCS([strlcat strlcpy])
 
 AC_SUBST([moduledir])
 
diff --git a/src/Makefile.am b/src/Makefile.am
index 504800d..65263cc 100644
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -23,7 +23,7 @@
 # -avoid-version prevents gratuitous .0.0.0 version numbers on the end
 # _ladir passes a dummy rpath to libtool so the thing will actually link
 # TODO: -nostdlib/-Bstatic/-lgcc platform magic, not installing the .a, etc.
-AM_CFLAGS = @XORG_CFLAGS@
+AM_CFLAGS = $(BASE_CFLAGS) $(XORG_CFLAGS)
 v4l_drv_la_LTLIBRARIES = v4l_drv.la
 v4l_drv_la_LDFLAGS = -module -avoid-version
 v4l_drv_ladir = @moduledir@/drivers
diff --git a/src/v4l.c b/src/v4l.c
index 8db8f7d..e0fdc45 100644
--- a/src/v4l.c
+++ b/src/v4l.c
@@ -86,7 +86,6 @@ _X_EXPORT XF86ModuleData v4lModuleData = { &v4lVersRec, v4lSetup, NULL };
 static pointer
 v4lSetup(pointer module, pointer opts, int *errmaj, int *errmin)
 {
-    const char *osname;
     static Bool setupDone = FALSE;
 
     if (setupDone) {
@@ -110,7 +109,7 @@ v4lSetup(pointer module, pointer opts, int *errmaj, int *errmin)
 #define VIDEO_OFF     0  /* really off */
 #define VIDEO_RGB     1  /* rgb overlay (directly to fb) */
 #define VIDEO_YUV     2  /* yuv overlay (to offscreen memory + hw scaling) */
-#define VIDEO_RECLIP  3  /* temporarly off, window clipping changes */
+#define VIDEO_RECLIP  3  /* temporarily off, window clipping changes */
 
 typedef struct _XvV4LCtrlRec {
     struct v4l2_queryctrl       qctrl;
@@ -189,7 +188,7 @@ static const XF86AttributeRec FreqAttr =
 static struct V4L_DEVICE {
     int  fd;
     int  useCount;
-    char devName[16];
+    char devName[18];
 } v4l_devices[MAX_V4L_DEVICES] = {
     { -1 },
     { -1 },
@@ -770,12 +769,15 @@ V4lSetPortAttribute(ScrnInfoPtr pScrn,
     } else if (attribute == xvFreq) {
         struct v4l2_frequency   freq;
         memset(&freq, 0, sizeof(freq));
-        ioctl(V4L_FD, VIDIOC_G_FREQUENCY, &freq);
-        freq.frequency = value;
-        if (ioctl(V4L_FD, VIDIOC_S_FREQUENCY, &freq) == -1)
-            xf86Msg(X_ERROR, "v4l: Error %d while setting frequency\n", errno);
-        else
-            ret = Success;
+        if (ioctl(V4L_FD, VIDIOC_G_FREQUENCY, &freq) == -1) {
+            xf86Msg(X_ERROR, "v4l: Error %d while getting frequency\n", errno);
+        } else {
+            freq.frequency = value;
+            if (ioctl(V4L_FD, VIDIOC_S_FREQUENCY, &freq) == -1)
+                xf86Msg(X_ERROR, "v4l: Error %d while setting frequency\n", errno);
+            else
+                ret = Success;
+        }
     } else {
         for (i = 0; i < pPPriv->n_qctrl; i++)
             if (pPPriv->XvV4LCtrl[i].xv == attribute)
@@ -906,16 +908,23 @@ static int
 AddV4LEnc(XF86VideoEncodingPtr enc, int entry,
             char *norm, char *input, int width, int height, int n, int d)
 {
-    enc->id     = entry;
-    enc->name   = malloc(strlen(norm) + strlen(input) + 2);
-    if (!enc->name)
+    char *name;
+
+#if ABI_VIDEODRV_VERSION >= SET_ABI_VERSION(10, 0)
+    if (Xasprintf(&name, "%s-%s", norm, fixname(input)) < 0)
+        name = NULL;
+#else
+    name = Xprintf("%s-%s", norm, fixname(input));
+#endif
+    if (name == NULL)
         return -1;
 
+    enc->id     = entry;
+    enc->name   = name;
     enc->width  = width;
     enc->height = height;
     enc->rate.numerator   = n;
     enc->rate.denominator = d * 2; /* Refresh rate is twice, due to interlace */
-    sprintf(enc->name,"%s-%s",norm,fixname(input));
 
     xf86Msg(X_INFO, "v4l: adding input %s, %dx%d %d fps\n",
             enc->name, enc->width, enc->height, (d + n - 1)/n);
@@ -1121,6 +1130,7 @@ V4LInit(ScrnInfoPtr pScrn, XF86VideoAdaptorPtr **adaptors)
     XF86VideoAdaptorPtr *VAR = NULL;
     char dev[18];
     int  fd,i,j,d;
+    void *tmp;
 
     for (i = 0, d = 0; d < MAX_V4L_DEVICES; d++) {
         sprintf(dev, "/dev/video%d", d);
@@ -1158,13 +1168,18 @@ V4LInit(ScrnInfoPtr pScrn, XF86VideoAdaptorPtr **adaptors)
         }
 
         xf86Msg(X_INFO, "v4l: enabling overlay mode for %s.\n", dev);
-        strncpy(V4L_NAME, dev, 16);
+        strncpy(V4L_NAME, dev, 18);
+        V4L_NAME[17] = '\0';
         V4LBuildEncodings(pPPriv, fd);
         if (NULL == pPPriv->enc)
             return FALSE;
 
         /* alloc VideoAdaptorRec */
-        VAR = realloc(VAR,sizeof(XF86VideoAdaptorPtr)*(i+1));
+        tmp = realloc(VAR,sizeof(XF86VideoAdaptorPtr)*(i+1));
+        if (!tmp)
+            return FALSE;
+        VAR = tmp;
+
         VAR[i] = malloc(sizeof(XF86VideoAdaptorRec));
         if (!VAR[i])
             return FALSE;
@@ -1186,7 +1201,7 @@ V4LInit(ScrnInfoPtr pScrn, XF86VideoAdaptorPtr **adaptors)
 
         /* Initialize yuv_format */
         if (0 != pPPriv->yuv_format) {
-            /* pass throuth scaler attributes */
+            /* pass through scaler attributes */
             for (j = 0; j < pPPriv->myfmt->num_attributes; j++) {
                 v4l_add_attr(&VAR[i]->pAttributes, &VAR[i]->nAttributes,
                              pPPriv->myfmt->attributes+j);
diff --git a/src/videodev2.h b/src/videodev2.h
index bf6dde2..c21b05a 100644
--- a/src/videodev2.h
+++ b/src/videodev2.h
@@ -511,7 +511,7 @@ struct v4l2_jpegcompression {
 	__u32 jpeg_markers;     /* Which markers should go into the JPEG
 				 * output. Unless you exactly know what
 				 * you do, leave them untouched.
-				 * Inluding less markers will make the
+				 * Including less markers will make the
 				 * resulting code smaller, but there will
 				 * be fewer applications which can read it.
 				 * The presence of the APP and COM marker
@@ -523,7 +523,7 @@ struct v4l2_jpegcompression {
 #define V4L2_JPEG_MARKER_DRI (1<<5)    /* Define Restart Interval */
 #define V4L2_JPEG_MARKER_COM (1<<6)    /* Comment segment */
 #define V4L2_JPEG_MARKER_APP (1<<7)    /* App segment, driver will
-					* allways use APP0 */
+					* always use APP0 */
 };
 
 /*
