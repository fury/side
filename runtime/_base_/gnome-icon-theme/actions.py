#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

WorkDir = "."

name = "gnome-icon-theme"
ver = get.srcVERSION()

def setup():
	# separate copyright
	shelltools.cd("%s-symbolic-%s" % (name, ver))
	for l in ["AUTHORS", "COPYING"]:
		shelltools.move(l, "%s-symbolic" % l)
	shelltools.cd("../%s-extras-%s" % (name, ver))
	for l in ["AUTHORS", "COPYING"]:
		shelltools.move(l, "%s-extras" % l)
	shelltools.cd("..")

	for i in ["%s-%s" % (name, ver), "%s-symbolic-%s" % (name, ver), "%s-extras-%s" % (name, ver)]:
		shelltools.cd(i)
		autotools.configure("--prefix=/usr")
		shelltools.cd("..")

def build():
	for i in ["%s-%s" % (name, ver), "%s-symbolic-%s" % (name, ver), "%s-extras-%s" % (name, ver)]:
		shelltools.cd(i)
		autotools.make()
		shelltools.cd("..")

def install():
	for i in ["%s-%s" % (name, ver), "%s-symbolic-%s" % (name, ver), "%s-extras-%s" % (name, ver)]:
		shelltools.cd(i)
		autotools.rawInstall("DESTDIR=%s" % get.installDIR())
		shelltools.cd("..")
