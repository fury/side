#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os

def postInstall(fromVersion, fromRelease, toVersion, toRelease):
	os.system("groupadd -g 88 unbound")
	os.system("useradd -c 'Unbound DNS Resolver' -m -d /var/lib/unbound -u 88 -g unbound -s /bin/false -k /dev/null -r unbound")

def postRemove():
	try:
		os.system("userdel -r unbound")
		os.system("groupdel unbound")
	except:
		pass
