#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from comar.service import *
import os

serviceType = "server"
serviceDesc = _({"en": "Unbound DNS resolver"})

serviceDefault = "off"

PIDFILE = "/run/unbound.pid"
ROOTKEYFILE="/etc/unbound/root.key"

@synchronized
def start():
	if not os.path.exists(ROOTKEYFILE):
		try:
			os.system("/usr/sbin/unbound-anchor")
		except:
			pass

	startService(command="/usr/sbin/unbound", donotify=True)

@synchronized
def stop():
	stopService(pidfile=PIDFILE, donotify=True)

	try:
		os.unlink(PIDFILE)
	except:
		pass

def status():
	return isServiceRunning(pidfile=PIDFILE)
