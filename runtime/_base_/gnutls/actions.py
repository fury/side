#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-manpages',
    ' --without-{zstd,brotli}',
    ' PYTHON=python3',
    ' --disable-doc',
    ' --disable-nls',
    ' --with-default-trust-store-pkcs11="pkcs11:"',
    ' --disable-static '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")
#	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	# remove empty dir
	pisitools.removeDir("/usr/share/man/man3")
