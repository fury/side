#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-features=huge',
    ' --with-tlib=ncursesw',
    ' --enable-gui=no'
    ])

def setup():
	shelltools.echo("src/feature.h", "#define SYS_VIMRC_FILE \"/etc/vim/vimrc\"")
	autotools.configure(i)

def build():
	autotools.make()

def check():
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dosym("vim", "/usr/bin/vi")

	pisitools.insinto("/etc/vim", "runtime/vimrc_example.vim", "vimrc")
	for t in ["applications", "icons"]:
		pisitools.removeDir("/usr/share/%s" % t)
