#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools

def setup():
	pisitools.dosed("makefile", "native", "x86-64")

def build():
	autotools.make()

def install():
	pisitools.dobin("unrar")
	pisitools.insinto("/usr/share/copyright/unrar", "license.txt")
