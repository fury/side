#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --disable-nls '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for b in shelltools.ls("%s/usr/share/doc/gnupg/" % get.installDIR()):
		if not b.startswith("example"):
			pisitools.remove("/usr/share/doc/gnupg/%s" % b)
