#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, docopyright

def build():
	pisitools.dosed("gptcurses.cc", "ncursesw\/", "")
	autotools.make("TARGET=Linux")

def install():
	for t in ["cgdisk", "fixparts", "gdisk", "sgdisk"]:
		pisitools.dosbin(t)
		pisitools.doman("%s.8" % t)

	docopyright.installCopyright()
