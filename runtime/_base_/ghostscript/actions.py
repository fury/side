#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --disable-compile-inits',
    ' --with-system-libtiff',
    ' --without-versioned-path '
    ])

def setup():
	for dirs in ["cups/libs", "freetype", "jpeg", "lcms2mt", "libpng", "openjpeg", "tiff", "zlib"]:
		shelltools.unlinkDir(dirs)

	autotools.configure(i)

def build():
	autotools.make()
	autotools.make("so")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	autotools.rawInstall("DESTDIR=%s" % get.installDIR(), "soinstall")

	for f in shelltools.ls("base/"):
		if f.endswith(".h"):
			pisitools.insinto("/usr/include/ghostscript", "base/%s" % f)
	pisitools.dosym("ghostscript", "/usr/include/ps")

	pisitools.insinto("/usr/share/ghostscript", "fonts")
	pisitools.insinto("/usr/share/copyright/ghostscript", "doc/COPYING")
	pisitools.removeDir("/usr/share/doc")
