#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

y = get.installDIR()

b = ''.join([
    ' DESTDIR=%s' % y,
    ' INSTALL_PREFIX=%s/usr' % y,
    ' INSTALL_DIR=%s/usr/sbin' % y,
    ' INSTALL_MANPAGES_DIR=%s/usr/share/man/man1' % y
    ])

def build():
	a = "{XZ,LZO,LZ4,ZSTD}_SUPPORT=1 COMP_DEFAULT=xz"
	autotools.make("%s -C squashfs-tools" % a)

def install():
	autotools.rawInstall("%s -C squashfs-tools" % b)
