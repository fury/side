#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools, pisitools

i = ''.join([
    ' -DENABLE_STATIC=OFF',
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DCMAKE_INSTALL_LIBDIR=lib',
    ' -DWITH_JPEG8=ON',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	pisitools.removeDir("/usr/share/doc")
