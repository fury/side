#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, shelltools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dlibaudit=no',
    ' -Dnmtui=true',
    ' -Dselinux=false',
    ' -Dsession_tracking=elogind',
    ' -Dmodem_manager=true',
    ' -Dsystemdsystemunitdir=no',
    ' -Dsystemd_journal=false',
    ' -Dbluez5_dun=true',
    ' -Dqt=false '
    ])

def setup():
	shelltools.system("grep -rl '^#!.*python$' | xargs sed -i '1s/python/&3/'")
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	shelltools.cd("man")
	for t in shelltools.ls("."):
		if t.endswith(tuple(['1','5','7','8'])):
			pisitools.doman(t)
