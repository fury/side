# -*- coding: utf-8 -*-
from comar.service import *
import os

serviceType = "local"
serviceDesc = _({"en": "Network Manager"})
serviceDefault = "conditional"

MSG_BACKEND_WARNING = _({"en" : "NetworkManager is not enabled by default. You can change this from /etc/conf.d/NetworkManager."})

PIDFILE="/run/NetworkManager/NetworkManager.pid"

@synchronized
def start():
  startService(command="/usr/sbin/NetworkManager", args="--pid-file=%s" % PIDFILE, donotify=True)

@synchronized
def stop():
  stopService(pidfile=PIDFILE, donotify=True)

  try:
    os.unlink(PIDFILE)
  except:
    pass

def ready():
  start()

def status():
  return isServiceRunning(pidfile=PIDFILE)
