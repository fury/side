#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

f = "75-yes-terminus.conf"

i = ''.join([
    ' --x11dir=/usr/share/fonts/misc',
    ' --otbdir=/usr/share/fonts/misc',
    ' --psfdir=/usr/share/kbd/consolefonts'
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make("all otb")

def install():
	autotools.rawInstall("DESTDIR=%s all otb" % get.installDIR())
	pisitools.insinto("/etc/fonts/conf.avail", f)
	for t in shelltools.ls("."):
		if t.endswith("otb"):
			pisitools.insinto("/usr/share/fonts/misc", t)

	pisitools.dosym("../conf.avail/%s" % f, "/etc/fonts/conf.d/%s" % f)

	pisitools.insinto("/usr/share/copyright/terminus-font", "OFL.TXT")
