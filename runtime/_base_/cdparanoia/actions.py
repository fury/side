#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

i = ''.join([
    ' --prefix=%s/usr' % get.installDIR(),
    ' --mandir=%s/usr/share/man' % get.installDIR()
    ])

def setup():
	shelltools.system("sed -i '/a\ $(LIBDIR)/d' Makefile.in")
	shelltools.system("sed -i '/so.0/s/644/755/g' Makefile.in")
	autotools.autoreconf("-vif")
	autotools.configure(i)

def build():
	autotools.make("-j1")

	
def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
