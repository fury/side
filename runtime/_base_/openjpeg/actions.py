#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, shelltools, mesontools, pisitools

i = ''.join([
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DBUILD_STATIC_LIBS=OFF',
    ' -B build -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	shelltools.cd("doc/man")
	for a in ["1", "3"]:
		for f in shelltools.ls("man%s" % a):
			if f.endswith(a):
				pisitools.doman("man%s/%s" % (a, f))
