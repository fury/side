#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

def setup():
	shelltools.system("./autogen.sh")
	autotools.configure("--prefix=/usr --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/etc", "conf/gpm-root.conf")
	i = "libgpm.so.2.1.0"
	pisitools.dosym(i, "/usr/lib/libgpm.so")
	shelltools.chmod("%s/usr/lib/%s" % (get.installDIR(), i), mode = 0755)
