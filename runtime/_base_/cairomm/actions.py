#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dbuild-tests=true',
    ' -Dboost-shared=true '
    ])

def setup():
	mesontools.configure(i, build_dir = "_build")

def build():
	mesontools.build(build_dir = "_build")

def check():
#	mesontools.build("test -C _build")
	pass

def install():
	mesontools.install(build_dir = "_build")
