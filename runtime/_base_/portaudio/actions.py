#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

WorkDir = "portaudio"

shelltools.export("JOBS", "-j1")

def setup():
	autotools.configure("--prefix=/usr --enable-cxx --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall('DESTDIR="%s" libdir=/usr/lib' % get.installDIR())
