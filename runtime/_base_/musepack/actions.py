#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get, docopyright

def setup():
	shelltools.system("find . -type f -exec chmod 644 {} \;")
	autotools.autoreconf("-fiv")
	autotools.configure("--prefix=/usr --enable-mpcchap --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	shelltools.cd("libmpcdec")
	docopyright.installCopyright()
