#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, docopyright, get

i = ''.join([
    ' --no-index',
    ' --no-user',
    ' --no-cache-dir',
    ' --no-compile',
    ' --find-links=dist',
    ' --root=%s' % get.installDIR()
    ])

def build():
	pythonmodules.run("-m flit_core.wheel", pyVer = "3")

def install():
	pythonmodules.run("-m pip install %s installer" % i, pyVer = "3")

	docopyright.installCopyright()
