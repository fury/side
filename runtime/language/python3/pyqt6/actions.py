#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

i = ''.join([
    ' --confirm-license',
    ' --qt-shared',
#    ' --disable QtDesigner',
#    ' --disable QtHelp',
    ' --no-make',
    ' --qmake=/usr/bin/qmake6 '
    ])

def setup():
	shelltools.system("sip-build %s" % i)

def build():
	autotools.make("-C build")

def install():
	autotools.rawInstall("INSTALL_ROOT=%s -C build" % get.installDIR())
