#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, pisitools, get

WorkDir = "flit-%s/flit_core" % get.srcVERSION()

whl = "dist/flit_core-%s-py3-none-any.whl" % get.srcVERSION()

def build():
	pythonmodules.run("-m flit_core.wheel", pyVer = "3")

def install():
	pythonmodules.run("bootstrap_install.py --install-root=%s %s" % (get.installDIR(), whl), pyVer = "3")
	pisitools.insinto("/usr/share/copyright/python3-flit-core", "LICENSE")
