#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, autotools, pisitools

def build():
	pythonmodules.compile(pyVer = "3")
	autotools.make("-C doc man")

def install():
	pythonmodules.install(pyVer = "3")

	pisitools.doman("doc/_build/man/pythondialog.1")
