#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

def postInstall(fromVersion, fromRelease, toVersion, toRelease):
  # Register XML::SAX::PurePerl
  os.system("/usr/bin/perl -MXML::SAX -e 'XML::SAX->add_parser(q(XML::SAX::PurePerl))->save_parsers()' 2>/dev/null")

def preRemove():
  # Unregister XML::SAX::PurePerl
  os.system("/usr/bin/perl -MXML::SAX -e 'XML::SAX->remove_parser(q(XML::SAX::PurePerl))->save_parsers()'")
