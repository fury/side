#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import perlmodules, pisitools

def setup():
	perlmodules.configure()

def build():
	perlmodules.make()

def check():
#	perlmodules.make("test")
	pass

def install():
	perlmodules.install()

	pisitools.insinto("/usr/lib/urxvt/perl", "misc/bidi")
