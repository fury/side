#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, pisitools, autotools, get

i = ''.join([
    ' --confirm-license',
    ' --qsci-api',
    ' --qmake=/usr/bin/qmake-qt5',
    ' --destdir="/usr/lib/python2.7/site-packages"',
    ' --sip-incdir="/usr/include/python2.7"',
    ' --sip /usr/bin/py2sip '
    ])

def setup():
	pisitools.dosed("sip/QtCore/QtCoremod.sip", ",\ py_ssize_t_clean=True", "")
	for h in ["configure.py", "project.py", "dbus/dbus.cpp"]:
		pisitools.dosed(h, "dbus-python.h", "dbus-python2.h")
	pythonmodules.run("configure.py %s" % i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("-C pyrcc INSTALL_ROOT=%s" % get.installDIR())
	autotools.rawInstall("-C pylupdate INSTALL_ROOT=%s" % get.installDIR())
	autotools.rawInstall("INSTALL_ROOT=%s" % get.installDIR())
	# Fix conflicts
	pisitools.rename("/usr/bin/pylupdate5", "py2lupdate5")
	pisitools.rename("/usr/bin/pyrcc5", "py2rcc5")    
	pisitools.rename("/usr/share/qt5/qsci/api/python/PyQt5.api", "Py2Qt5.api")
	pisitools.rename("/usr/bin/pyuic5", "py2uic5")    
	pisitools.rename("/usr/share/sip/PyQt5", "Py2Qt5")
