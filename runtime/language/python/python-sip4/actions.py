#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, autotools, pisitools, get

command = "configure.py CFLAGS+='%s' CXXFLAGS+='%s'" % (get.CFLAGS(), get.CXXFLAGS())

def setup():
	pythonmodules.run("%s --sip-module PyQt5.sip" % command)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.rename("/usr/bin/sip", "py2sip")
