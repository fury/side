#!/usr/bin/python2

import os

def postInstall(fromVersion, fromRelease, toVersion, toRelease):
    os.environ["LC_ALL"] = "C"
    os.environ["PATH"] = "/usr/sbin:/usr/bin"
    try:
        os.system("/usr/sbin/grub-mkconfig -o /boot/grub/grub.cfg")
    except:
        pass

def preRemove():
    pass
