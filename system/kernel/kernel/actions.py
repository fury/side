#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import kerneltools, shelltools, pisitools, get

NoStrip = ["/usr/lib", "/boot"]

shelltools.export("KBUILD_BUILD_USER", "side")
shelltools.export("KBUILD_BUILD_HOST", "side")
shelltools.export("PYTHONDONTWRITEBYTECODE", "1")

def setup():
#	shelltools.system("make menuconfig")
	kerneltools.configure()

def build():
	kerneltools.build(debugSymbols=False)

def install():
	kerneltools.install()

	# add objtool for external module building and enabled VALIDATION_STACK option
	pisitools.insinto("/usr/src/linux-headers-%s/tools/objtool" % get.srcVERSION(), "%s/tools/objtool/objtool" % get.curDIR())

	# Install kernel headers needed for out-of-tree module compilation
	kerneltools.installHeaders()
	kerneltools.installLibcHeaders()

	# Generate some module lists to use within mkinitramfs
	shelltools.system("./generate-module-list %s/usr/lib/modules/%s" % (get.installDIR(), kerneltools.__getSuffix()))

	# added copyrights
	l1 = "LICENSES/preferred/GPL-2.0"
	l2 = "LICENSES/exceptions/Linux-syscall-note"
	l3 = "Documentation/process/license-rules.rst"
	for l in [l1, l2, l3]:
		pisitools.insinto("/usr/share/copyright/kernel", l)
