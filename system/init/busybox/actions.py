#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, docopyright

def build():
	autotools.make()
	autotools.make("busybox.links")

def install():
	pisitools.dobin("busybox")
	pisitools.insinto("/usr/bin", "busybox.links")

	docopyright.installCopyright()
