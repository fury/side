#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

def build():
	shelltools.export("LDFLAGS", "")
	autotools.make("PYTHON=python2 efi64 efi32 installer")

def install():
	b = "SBINDIR=/usr/sbin MANDIR=/usr/share/man bios efi64 efi32"
	autotools.rawInstall("INSTALLROOT=%s %s" % (get.installDIR(), b))
