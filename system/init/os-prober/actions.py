#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pisitools, shelltools, autotools

def build():
	autotools.make("newns")

def install():
	for i in ["linux-boot-prober", "os-prober"]:
		pisitools.dobin(i)

	pisitools.doexe("newns", "/usr/lib/os-prober/")
	pisitools.doexe("common.sh", "/usr/share/os-prober/")

	for t in ["os-probes", "os-probes/mounted", "os-probes/init", "linux-boot-probes", "linux-boot-probes/mounted"]:
		pisitools.insinto("/usr/lib/%s" % t, "%s/common/*" % t)
		if shelltools.isDirectory("%s/x86" % t):
			pisitools.insinto("/usr/lib/%s" % t, "%s/x86/*" % t)
	pisitools.insinto("/usr/lib/os-probes/mounted", "os-probes/mounted/powerpc/20macosx")

	pisitools.dodir("/var/lib/os-prober")
