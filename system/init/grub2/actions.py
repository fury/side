#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --disable-efiemu',
    ' --disable-werror '
    ])

def setup():
	pisitools.cflags.remove("-fstack-protector", "-fasynchronous-unwind-tables", "-fexceptions", "-fPIC")
	pisitools.cflags.sub("\s?(-O[\ds]|-D_FORTIFY_SOURCE=\d)\s?", " ")

	shelltools.echo("grub-core/extra_deps.lst", "depends bli part_gpt")
	shelltools.system("./autogen.sh")

	for t in ["pc", "efi"]:
		shelltools.makedirs(t)
		shelltools.cd(t)
		shelltools.system("../configure PYTHON=python3 --with-platform=%s %s" % (t, i))
		shelltools.cd("..")

def build():
	for t in ["pc", "efi"]:
		shelltools.cd(t)
		autotools.make()
		shelltools.cd("..")

def install():
	shelltools.system("./pc/grub-mkfont -o unicode.pf2 ../unifont-15.1.05.bdf")
	pisitools.insinto("/usr/share/grub", "unicode.pf2")

	for t in ["pc", "efi"]:
		shelltools.cd(t)
		autotools.rawInstall("DESTDIR=%s" % get.installDIR())
		shelltools.cd("..")
