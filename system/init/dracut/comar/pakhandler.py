# -*- coding: utf-8 -*-

import os
import piksemel
import subprocess

def generate_initramfs(filepath):
    patterns = ("boot/kernel")
    doc = piksemel.parse(filepath)
    for item in doc.tags("File"):
        path = item.getTagData("Path")
        if path.startswith(patterns):
            kernel = open("/etc/kernel/kernel", "r")
            kver = kernel.read()
            for i in kver.splitlines():
                subprocess.call(["/usr/sbin/depmod", kver])
                subprocess.call(["/usr/bin/dracut", "-f", "--kver", kver])
            return

def setupPackage(metapath, filepath):
    generate_initramfs(filepath)

def cleanupPackage(metapath, filepath):
    pass

def postCleanupPackage(metapath, filepath):
    pass
