#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' prefix=%s/usr' % get.installDIR(),
    ' MANDIR=%s/usr/share/man/man1' % get.installDIR()
    ])

def build():
	autotools.make("-f unix/Makefile generic")

def install():
	autotools.make("-f unix/Makefile %s install" % i)

	pisitools.insinto("/usr/share/copyright/unzip", "LICENSE")
