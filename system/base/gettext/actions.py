#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

def setup():
	autotools.configure("--prefix=/usr --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	shelltools.chmod("%s/usr/lib/preloadable_libintl.so" % get.installDIR(), mode = 0755)
	pisitools.removeDir("/usr/share/doc")
