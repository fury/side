#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-python{,3}=no',
    ' --disable-{zos-remote,static} '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")

def install():
	autotools.rawInstall('DESTDIR="%s"' % get.installDIR())

	# remove RH specific bits
	pisitools.removeDir("/etc/sysconfig")
	pisitools.removeDir("/etc/rc.d")

	# Create data directories
	pisitools.dodir("/var/log/audit")
	pisitools.dodir("/var/spool/audit")

	a = "audit.rules"
	pisitools.insinto("/etc/audit/rules.d", "rules/10-no-%s" % a, a)
