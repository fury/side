#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pisitools, cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DLEVELDB_BUILD_TESTS=OFF',
    ' -DLEVELDB_BUILD_BENCHMARKS=OFF',
    ' -DBUILD_SHARED_LIBS=1',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	pisitools.dosed("CMakeLists.txt", "fno-rtti", deleteLine = True)
	cmaketools.configure(i)

def build():
	mesontools.build()

def check():
	pass

def install():
	mesontools.install()
