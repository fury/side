#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, shelltools, pisitools

shelltools.export("PYCURL_SSL_LIBRARY", "openssl")

def install():
	pisitools.dosed("setup.py", ", \"--static-libs\"")

	pythonmodules.install("--curl-config=/usr/bin/curl-config --prefix=/usr --optimize=1")
	pisitools.removeDir("/usr/share/doc")
