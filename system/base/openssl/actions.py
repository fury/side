#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --openssldir=/etc/ssl',
    ' --libdir=lib',
    ' shared',
    ' zlib-dynamic '
    ])

def setup():
	shelltools.system("./config %s" % i)

def build():
	autotools.make()

def check():
#	autotools.make("HARNESS_JOBS=$(nproc) test")
	pass

def install():
	shelltools.system("sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile")
	autotools.rawInstall("DESTDIR=%s MANSUFFIX=ssl" % get.installDIR())

	pisitools.removeDir("/usr/share/doc")
