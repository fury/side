#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --enable-elf-shlibs',
    ' --disable-libblkid',
    ' --disable-libuuid',
    ' --disable-uuidd',
    ' --disable-fsck',
    ' --disable-nls '
    ])

def setup():
	shelltools.makedirs("build")
	shelltools.cd("build")
	shelltools.system("../configure %s" % i)

def build():
	shelltools.cd("build")
	autotools.make()

def check():
	shelltools.cd("build")
	autotools.make("check")

def install():
	shelltools.cd("build")
	autotools.rawInstall("DESTDIR=%s LDCONFIG=/bin/true" % get.installDIR())

	pisitools.remove("/usr/lib/*.a")
