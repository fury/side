#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --datadir=/usr/share/kbd',
    ' --disable-{nls,vlock} '
    ])

def setup():
	pisitools.dosed("configure", "RESIZECONS_PROGS=yes", "RESIZECONS_PROGS=no")
	pisitools.dosed("docs/man/man8/Makefile.in", "resizecons\.8\ ", "")
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
