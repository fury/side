#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' PYTHON_VERSION=2',
    ' --prefix=/usr',
    ' --disable-documentation',
    ' --disable-static '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")
#	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.rename("/usr/include/dbus-1.0/dbus/dbus-python.h", "dbus-python2.h")
	pisitools.rename("/usr/lib/pkgconfig/dbus-python.pc", "dbus-python2.pc")
