#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

WorkDir = "nspr-%s/nspr" % get.srcVERSION()

i = ''.join([
    ' --prefix=/usr',
    ' --enable-64bit',
    ' --with-mozilla',
    ' --with-pthreads',
    ' --disable-debug '
    ])

def setup():
	pisitools.dosed("config/rules.mk", "\$\(LIBRARY\)\ ", "")
	pisitools.dosed("pr/src/misc/Makefile.in", "^RELEASE", "#RELEASE")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
