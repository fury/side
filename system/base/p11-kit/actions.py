#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dman=true',
    ' -Dnls=false',
    ' -Dtrust_paths=/etc/pki/anchors '
    ])

def setup():
	mesontools.configure(i, build_dir="_build")

def build():
	mesontools.build("-C _build")

def check():
#	mesontools.build("-C _build test")
	pass

def install():
	mesontools.install("-C _build")
