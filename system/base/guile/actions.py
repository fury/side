#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --disable-nls',
    ' --enable-posix',
    ' --enable-regex',
    ' --with-threads',
    ' --with-modules',
    ' --disable-rpath',
    ' --disable-static',
    ' --enable-networking',
    ' --disable-error-on-warning '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")
#	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
