#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, get

NoStrip = ["/usr/lib"]

def setup():
	# fix alsa-firmware file conflicts
	for fw in ("ess", "korg", "sb16", "yamaha"):
		shelltools.unlinkDir(fw)
	shelltools.unlink("ctspeq.bin")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s FIRMWAREDIR=/usr/lib/firmware" % get.installDIR())
