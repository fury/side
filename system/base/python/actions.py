#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-ipv6',
    ' --enable-shared',
    ' --enable-unicode=ucs4',
    ' --with-system-expat',
    ' --with-system-ffi',
    ' --with-threads',
    ' --with-ensurepip=no',
    ' --with-dbmliborder=gdbm '
    ])

def setup():
	shelltools.unlinkDir("Modules/expat")
	shelltools.unlinkDir("Modules/zlib")
	shelltools.unlinkDir("Modules/_ctypes/libffi")

	autotools.configure(i)

def build():
	autotools.make()

def check():
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR(), "altinstall")

	pisitools.dosym("python2.7", "/usr/bin/python")
	pisitools.dosym("python2.7", "/usr/bin/python2")
	pisitools.dosym("python2.7-config", "/usr/bin/python-config")
	pisitools.dosym("/usr/lib/python2.7/pdb.py", "/usr/bin/pdb")
