#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

WorkDir="icu/source"

i = ''.join([
    ' --prefix=/usr',
    ' --disable-static',
    ' --disable-tests',
    ' --disable-samples '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("-k check || true")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/usr/share/copyright/icu4c", "../LICENSE")
