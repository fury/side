#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' PYTHON=/usr/bin/python3',
    ' --prefix=/usr',
    ' --disable-nls',
    ' --disable-static',
    ' --with-default-dict=/usr/lib/cracklib/pw_dict '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("test")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/usr/share/dict", "cracklib-words-2.10.2", "cracklib-words")
	pisitools.dosym("cracklib-words", "/usr/share/dict/words")

	pisitools.dodir("/usr/lib/cracklib")
	for a in ["hwm", "pwd", "pwi"]:
		shelltools.touch("%s/usr/lib/cracklib/pw_dict.%s" % (get.installDIR(), a))
