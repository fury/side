#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-utf8',
    ' --enable-altrcname',
    ' --enable-threads=posix '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dosed("doc/sample.nanorc", "^#\ include", "include")
	pisitools.dosed("doc/sample.nanorc", "^#\ set\ linenumbers", "set linenumbers")

	pisitools.insinto("/etc/", "doc/sample.nanorc", "nanorc")
	pisitools.removeDir("/usr/share/doc")
