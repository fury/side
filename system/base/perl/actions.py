#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import perlmodules, shelltools, autotools, pisitools, get

shelltools.export("BUILD_ZLIB", "False")
shelltools.export("BUILD_BZIP2", "0")

i = ''.join([
    ' -des',
    ' -Dcc=gcc',
    ' -Dprefix=/usr',
    ' -Dvendorprefix=/usr',
    ' -Dprivlib=/usr/lib/perl5/5.38/core_perl',
    ' -Darchlib=/usr/lib/perl5/5.38/core_perl',
    ' -Dsitelib=/usr/lib/perl5/5.38/site_perl',
    ' -Dsitearch=/usr/lib/perl5/5.38/site_perl',
    ' -Dvendorlib=/usr/lib/perl5/5.38/vendor_perl',
    ' -Dvendorarch=/usr/lib/perl5/5.38/vendor_perl',
    ' -Dman1dir=/usr/share/man/man1',
    ' -Dman3dir=/usr/share/man/man3',
    ' -Dpager="/usr/bin/less -isR"',
    ' -Duseshrplib',
    ' -Dusethreads '
    ])

def setup():
	shelltools.system("sh Configure %s" % i)

def build():
	autotools.make()

def check():
	pass
#	autotools.make("-j1 test")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	shelltools.system("unset BUILD_ZLIB")
	shelltools.system("unset BUILD_BZIP2")
