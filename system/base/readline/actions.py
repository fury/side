#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-curses',
    ' --disable-static '
    ])

def setup():
	shelltools.system("sed -i '/MV.*old/d' Makefile.in")
	shelltools.system("sed -i '/{OLDSUFF}/c:' support/shlib-install")
	shelltools.system("sed -i 's/-Wl,-rpath,[^ ]*//' support/shobj-conf")
	autotools.configure(i)

def build():
	autotools.make("SHLIB_LIBS='-lncursesw'")

def install():
	autotools.rawInstall("DESTDIR=%s SHLIB_LIBS='-lncursesw'" % get.installDIR())

	for a in ["/usr/bin", "/usr/share/doc"]:
		pisitools.removeDir(a)
