#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --disable-{nls,static} '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for a in ["COPYING", "COPYING.LGPL"]:
		pisitools.insinto("/usr/share/copyright/acl", "doc/%s" % a)
	pisitools.removeDir("/usr/share/doc")
