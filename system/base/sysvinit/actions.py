#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

i = ''.join([
    ' USRBIN=\'\'',
    ' SBIN=\'init halt shutdown runlevel killall5 fstab-decode bootlogd\'',
    ' {bindir,base_bindir,base_sbindir}=/usr/sbin',
    ' MAN8=\'halt.8 init.8 killall5.8 poweroff.8 reboot.8\'',
    ' MAN8+=\'runlevel.8 shutdown.8 telinit.8 fstab-decode.8 bootlogd.8\'',
    ' MAN1=\'\'',
    ' ROOT=%s ' % get.installDIR()
    ])

def build():
	autotools.make()

def install():
	autotools.rawInstall(i)
