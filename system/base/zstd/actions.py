#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, mesontools

y = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' --default-library=shared',
    ' -Dbin_programs=true',
    ' -Dbin_{tests,contrib}=false',
    ' build/meson '
    ])

def setup():
	# fix release manpages
	shelltools.system("patch -p1 -i ../man157.patch")
	mesontools.configure(y, build_dir = "_build_")

def build():
	mesontools.build("-C _build_")

def check():
	mesontools.build("-C _build_ test")

def install():
	mesontools.install("-C _build_")
