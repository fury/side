#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-cxx',
    ' --enable-dbm',
    ' --enable-stl',
    ' --enable-compat185',
    ' --enable-dumb185',
    ' --disable-java',
    ' --disable-tcl',
    ' --bindir=/usr/bin',
    ' --libdir=/usr/lib',
    ' --datadir=/usr/share',
    ' --includedir=/usr/include',
    ' --disable-static '
    ])

def setup():
	shelltools.cd("build_unix")
	shelltools.system("../dist/configure %s" % i)

def build():
	autotools.make("-C build_unix")

def install():
	autotools.rawInstall("DESTDIR=%s -C build_unix" % get.installDIR())
