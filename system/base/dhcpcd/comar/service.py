#!/usr/bin/python
# -*- coding: utf-8 -*-

from comar.service import *
import os
import socket

serviceType = "local"
serviceDefault = "conditional"
serviceDesc = _({"en": "Dhcp client"})

MSG_BACKEND_WARNING = _({"en" : "dhcp is not enabled by default. You can change this from /etc/dhcpcd.conf."})

pidfile="/run/dhcpcd/pid"
USETHIS=eval(config.get("DEFAULT", "True"))
HOSTNAME=(socket.gethostname())

@synchronized
def start():
    if not USETHIS:
        fail(MSG_BACKEND_WARNING)

    startService(command="/usr/sbin/dhcpcd",
                 args="-b -q -h %s " % HOSTNAME,
                 donotify=True)

@synchronized
def stop():
    stopService(command="/usr/sbin/dhcpcd -k",
                donotify=True)

    try:
        os.unlink(pidfile)
    except:
        pass

def ready():
    if not USETHIS:
        fail(MSG_BACKEND_WARNING)
    else:
        start()

def status():
    return isServiceRunning(pidfile=pidfile)
