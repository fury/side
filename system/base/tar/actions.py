#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, get

def setup():
	shelltools.export("FORCE_UNSAFE_CONFIGURE", "1")
	autotools.configure("--prefix=/usr")

def build():
	autotools.make()

def check():
#	autotools.make("check")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
