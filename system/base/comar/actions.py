#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, autotools, pisitools, get

i = ''.join([
    ' -DPYTHON_INCLUDE_DIR=/usr/include/python2.7',
    ' -DPYTHON_LIBRARY=/usr/lib/libpython2.7.so '
    ])

def setup():
	pisitools.dosed("CMakeLists.txt", "etc/dbus", "usr/share/dbus")
	cmaketools.configure(i)

def build():
	autotools.make("VERBOSE=1")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
