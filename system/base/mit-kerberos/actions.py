#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

WorkDir = "krb5-%s/src" % get.srcVERSION()

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --localstatedir=/var/lib',
    ' --runstatedir=/run',
    ' --enable-dns-for-realm',
    ' --with-system-{et,ss}',
    ' --with-system-verto=no',
    ' --disable-nls '
    ])

def setup():
	shelltools.system("sed -i -e '/eq 0/{N;s/12 //}' plugins/kdb/db2/libdb2/test/run.test")
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("-j1 -k check")
#	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/usr/share/copyright/mit-kerberos", "../NOTICE")
