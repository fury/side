#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pythonmodules, shelltools, pisitools

def build():
	pisitools.dosed("setup.py", "self.installdoc", deleteLine = True)
	pythonmodules.compile(pyVer = "2")

def install():
	pythonmodules.install(pyVer = "2")

	pisitools.dosym("pisi-cli", "/usr/bin/pisi")

	shelltools.touch("LOCK")
	shelltools.chmod("LOCK", 0o666)
	pisitools.dodir("/run/lock/files.ldb")
	pisitools.insinto("/run/lock/files.ldb", "LOCK")
	pisitools.dodir("/var/lib/pisi/info/files.ldb")
