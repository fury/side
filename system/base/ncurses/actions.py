#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --mandir=/usr/share/man',
    ' --enable-{pc-files,widec}',
    ' --with-{shared,cxx-shared}',
    ' --without-{debug,normal}',
    ' --with-pkg-config-libdir=/usr/lib/pkgconfig '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for lib in ["ncurses", "ncurses++", "form", "panel", "menu"]:
		shelltools.echo("lib%s.so" % lib, "INPUT(-l%sw)" % lib)
		pisitools.dolib_so("lib%s.so" % lib, destinationDirectory = "/usr/lib")

	shelltools.echo("libcursesw.so", "INPUT(-lncursesw)")
	pisitools.dolib_so("libcursesw.so", destinationDirectory = "/usr/lib/")
	pisitools.dosym("libncurses.so", "/usr/lib/libcurses.so")

	for lib in ["ncurses", "ncurses++", "form", "panel", "menu"]:
		pisitools.dosym("%sw.pc" % lib, "/usr/lib/pkgconfig/%s.pc" % lib)
