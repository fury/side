#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_INSTALL_PREFIX=/usr',
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DBUILD_STATIC_LIBS=OFF',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def check():
	mesontools.build("test")

def install():
	mesontools.install()
