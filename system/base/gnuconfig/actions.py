#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools

def build():
	autotools.make()

def check():
	autotools.make("check")

def install():
	pisitools.doexe("config.guess", "/usr/share/gnuconfig")
	pisitools.doexe("config.sub", "/usr/share/gnuconfig")
