#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get, docopyright

def build():
	autotools.make("-f unix/Makefile CC=%s CPP=%s generic_gcc" % (get.CC(), get.CXX()))

def install():
	for bins in ["zip", "zipcloak", "zipnote", "zipsplit"]:
		pisitools.dobin(bins)

	pisitools.doman("man/*.1")
	docopyright.installCopyright()
