#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --disable-werror',
    ' --enable-kernel=4.19',
    ' --enable-stack-protector=strong',
    ' --disable-nscd',
    ' libc_cv_slibdir=/usr/lib '
    ])

def setup():
	shelltools.makedirs("build")
	shelltools.cd("build")
	shelltools.echo("configparms", "rootsbindir=/usr/sbin")
	shelltools.system("ln -sfv ../configure ./configure")
	autotools.configure(i)

def build():
	shelltools.cd("build")
	autotools.make()

def install():
	shelltools.cd("build")
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	shelltools.system("sed '/RTLDLIST=/s@/usr@@g' -i %s/usr/bin/ldd" % get.installDIR())
	for dir__t in ["/etc/ld.so.conf.d", "/usr/lib/locale"]:
		pisitools.dodir(dir__t)

	for c in ["CONTRIBUTED-BY", "COPYING", "COPYING.LIB"]:
		pisitools.insinto("/usr/share/copyright/glibc", "../%s" % c)
