#!/usr/bin/python2

import os

def postInstall(fromVersion, fromRelease, toVersion, toRelease):
    if os.path.exists("/etc/locale.gen"):
        os.system("/usr/bin/locale-gen")
    else:
        os.system("localedef -i C -f UTF-8 C.UTF-8")
        os.system("localedef -i en_US -f UTF-8 en_US.UTF-8")
