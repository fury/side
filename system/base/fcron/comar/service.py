from comar.service import *
import os

serviceType = "local"
serviceDesc = _({"en": "A periodical command scheduler service."})
serviceDefault = "on"

pidfile = "/run/fcron.pid"

@synchronized
def start():
  startService(command="/usr/sbin/fcron",
               detach=True,
               pidfile=pidfile,
               donotify=True)

  if not os.path.isfile("/var/spool/fcron/systab"):
    try:
      os.system("fcrontab -z -u systab")
    except:
      pass

@synchronized
def stop():
  stopService(command="/usr/sbin/fcron",
              donotify=True)

def status():
  return isServiceRunning(pidfile)
