#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-pam=yes',
    ' --with-piddir=/run',
    ' --with-editor=/usr/bin/nano',
    ' --with-systemdsystemunitdir=no',
    ' --with-boot-install=no',
    ' --with-sendmail=no'
    ])

def setup():
	shelltools.system("find doc -type f -exec sed -i 's:/usr/local::g' {} \;")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	pisitools.dodir("/etc/pam.d")
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for t in ["/usr/share/doc", "/var/run"]:
		pisitools.removeDir(t)
