#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pisitools, shelltools, get

WorkDir = "."

ZoneDir = "/usr/share/zoneinfo"
TargetDir = "%s%s" % (get.installDIR(), ZoneDir)

RightDir = "%s/right" % TargetDir
PosixDir = "%s/posix" % TargetDir

timezones = ["etcetera", "southamerica", "northamerica", "europe",
"africa", "antarctica", "asia", "australasia", "factory", "backward"]

def setup():
	pisitools.dodir (ZoneDir)
	pisitools.dodir (RightDir)
	pisitools.dodir (PosixDir)

def install():
	pisitools.insinto ("/usr/share/zoneinfo", "iso3166.tab")
	pisitools.insinto ("/usr/share/zoneinfo", "zone.tab")
	pisitools.insinto ("/usr/share/zoneinfo", "zone1970.tab")

	for tzdata in timezones:
		cmd = "zic -L /dev/null -d %s -y \"%s/yearistype.sh\" %s" % (TargetDir, get.workDIR(), tzdata)
		shelltools.system(cmd)
		part2 = "zic -L /dev/null -d %s -y \"%s/yearistype.sh\" %s" % (PosixDir, get.workDIR(), tzdata)
		shelltools.system(part2)
		part3 = "zic -L leapseconds -d %s -y \"%s/yearistype.sh\" %s" % (RightDir, get.workDIR(), tzdata)
		shelltools.system(part3)

	shelltools.system("zic -d %s -p Europe/London" % TargetDir)
