#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --enable-{gold,shared,plugins}',
    ' --enable-ld=default',
    ' --enable-64-bit-bfd',
    ' --enable-default-hash-style=gnu',
    ' --disable-{nls,werror}',
    ' --with-zstd=no',
    ' --with-system-zlib '
    ])

def setup():
	shelltools.makedirs("build")
	shelltools.cd("build")
	shelltools.system("../configure %s" % i)

def build():
	autotools.make("-C build tooldir=/usr")

def check():
#	autotools.make("-C build -k check")
	pass

def install():
	autotools.rawInstall("-C build DESTDIR=%s tooldir=/usr" % get.installDIR())

	for a in ["bfd", "ctf", "ctf-nobfd", "gprofng", "sframe", "opcodes"]:
		pisitools.remove("/usr/lib/lib%s.a" % a)
