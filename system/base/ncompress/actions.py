#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools

def build():
	shelltools.move("Makefile.def", "Makefile")
	autotools.make("-f Makefile")

def install():
	pisitools.dobin("compress")
	pisitools.doman("compress.1")

	for i in ["LZW.INFO", "UNLICENSE"]:
		pisitools.insinto("/usr/share/copyright/ncompress", i)
