#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

dirs = ("dict", "rfc")

def setup():
	autotools.configure("--datadir=/usr/share/misc")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	for d in dirs:
		pisitools.dodir("/usr/share/%s" % d)
		for line in tuple(open("Makefile.am", 'r')):
			if line.startswith(d):
				for f in line.strip().split("=")[1][1:].split(" "):
					pisitools.domove("/usr/share/misc/%s" % f, "/usr/share/%s/" % d)
