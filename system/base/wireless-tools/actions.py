#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

a = ''.join([
    ' DESTDIR=%s' % get.installDIR(),
    ' PREFIX=%s/usr' % get.installDIR(),
    ' INSTALL_MAN=%s/usr/share/man ' % get.installDIR()
    ])

def setup():
	pisitools.dosed("Makefile", "LDCONFIG", deleteLine = True)

def build():
	autotools.make()

def install():
	autotools.rawInstall(a)
