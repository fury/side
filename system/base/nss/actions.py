#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get, docopyright

WorkDir = "nss-%s/nss" % get.srcVERSION()

i = ''.join([
    ' BUILD_OPT=1',
    ' NSPR_INCLUDE_DIR=/usr/include/nspr',
    ' USE_SYSTEM_ZLIB=1',
    ' ZLIB_LIBS=-lz',
    ' NSS_ENABLE_WERROR=0',
    ' USE_64=1',
    ' NSS_USE_SYSTEM_SQLITE=1 '
    ])

def build():
	autotools.make(i)
	autotools.make("-C doc")

def install():
	pisitools.dolib_so("../dist/Linux*/lib/*.so")
	pisitools.dolib("../dist/Linux*/lib/*.chk")
	pisitools.dolib_a("../dist/Linux*/lib/libcrmf.a")

	for a in ["public/nss", "private/nss"]:
		pisitools.insinto("/usr/include/nss", "../dist/%s/*.h" % a, sym = False)

	for b in ["certutil", "nss-config", "pk12util"]:
		pisitools.dobin("../dist/Linux*/bin/%s" % b)

	pisitools.insinto("/usr/lib/pkgconfig", "../dist/Linux*/lib/pkgconfig/nss.pc", sym = False)
	shelltools.chmod("%s/usr/lib/*.chk" % get.installDIR(), mode = 0644)

	for m in ["doc/nroff/certutil.1", "doc/nroff/pk12util.1"]:
		pisitools.doman(m)

	docopyright.installCopyright()
