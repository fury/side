#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' prefix=/usr',
    ' lib=lib',
    ' SBINDIR=/usr/sbin',
    ' PAM_CAP=yes',
    ' GOLANG=no',
    ' RAISE_SETFCAP=no '
    ])

def setup():
	pisitools.dosed("libcap/Makefile", "install -m.*STA", deleteLine = True)

def build():
	autotools.make(i)

def check():
#	autotools.make("-k test")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s prefix=/usr lib=lib" % get.installDIR())

	pisitools.insinto("/usr/share/examples/libcap", "pam_cap/capability.conf")
