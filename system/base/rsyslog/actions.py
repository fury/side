#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --mandir=/usr/share/man',
    ' --enable-omstdout',
    ' --enable-gssapi-krb5',
    ' --enable-openssl',
    ' --enable-mail',
    ' --enable-omprog',
    ' --enable-omuxsock',
    ' --enable-imfile',
    ' --enable-imdiag',
    ' --enable-imptcp',
    ' --enable-impstats',
    ' --enable-imdocker',
    ' --enable-mmanon',
    ' --enable-mmaudit',
    ' --enable-mmjsonparse',
    ' --enable-pmlastmsg',
    ' --enable-unlimited-select',
    ' --enable-generate-man-pages',
    ' --disable-libsystemd '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dodir("/var/spool/rsyslog")
	pisitools.dodir("/var/empty/dev")
