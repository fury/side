#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

def setup():
	pisitools.dosed("request-key.conf", "/bin/keyctl", "/usr/bin/keyctl")
	pisitools.dosed("request-key.conf", "/sbin", "/usr/sbin")
	pisitools.dosed("Makefile", "\$\(LNS\)\ \$\(LIBDIR\)/\$\(SONAME\)", "$(LNS) $(SONAME)")

def build():
	autotools.make()

def install():
	autotools.install("NO_ARLIB=1 LIBDIR=/usr/lib BINDIR=/usr/bin SBINDIR=/usr/sbin DESTDIR=%s" % get.installDIR())

	shelltools.chmod("%s/usr/lib/pkgconfig/libkeyutils.pc" % get.installDIR(), mode = 0644)

	for l in ["LICENCE.GPL", "LICENCE.LGPL"]:
		pisitools.insinto("/usr/share/copyright/keyutils", l)
