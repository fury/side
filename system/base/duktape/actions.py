#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

a = "-f Makefile.sharedlibrary INSTALL_PREFIX=/usr"

def setup():
	pisitools.dosed("Makefile.sharedlibrary", "Os", "O2")

def build():
	autotools.make(a)

def install():
	autotools.rawInstall("%s DESTDIR=%s" % (a, get.installDIR()))
