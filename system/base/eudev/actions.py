#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --bindir=/usr/sbin',
    ' --sysconfdir=/etc',
    ' --enable-{kmod,manpages}',
    ' --disable-static '
    ])

def setup():
	autotools.autoreconf("-fiv")
	autotools.configure(i)

def build():
	autotools.make()

def check():
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
