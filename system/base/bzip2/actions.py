#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	pisitools.dosed("Makefile", "\(PREFIX\)/man", "(PREFIX)/share/man")

def build():
	autotools.make("-f Makefile-libbz2_so")
	autotools.make("clean")

def check():
	autotools.make("check")

def install():
	autotools.rawInstall("PREFIX=%s/usr" % get.installDIR())

	for l in ["bzcmp", "bzegrep", "bzfgrep", "bzless", "bzcat", "bunzip2"]:
		pisitools.remove("/usr/bin/%s" % l)

	pisitools.dosym("bzip2", "/usr/bin/bzcat")
	pisitools.dosym("bzip2", "/usr/bin/bunzip2")
	pisitools.dosym("bzgrep", "/usr/bin/bzegrep")
	pisitools.dosym("bzgrep", "/usr/bin/bzfgrep")
	pisitools.dosym("bzdiff", "/usr/bin/bzcmp")
	pisitools.dosym("bzmore", "/usr/bin/bzless")

	pisitools.dolib_so("libbz2.so.1.0.8")
	for so in ["libbz2.so.1.0", "libbz2.so.1", "libbz2.so"]:
		pisitools.dosym("libbz2.so.1.0.8", "/usr/lib/%s" % so)

	pisitools.remove("/usr/lib/libbz2.a")
