#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, shelltools, pisitools, get

a = "%s/%s" % (get.workDIR(), get.srcDIR().replace("-", ""))

WorkDir = "tcl%s/unix" % get.srcVERSION()

def setup():
	shelltools.unlinkDir("%s/tcl8.6.15/pkgs/sqlite3.45.3" % get.workDIR())

	autotools.configure("--prefix=/usr --enable-64bit --mandir=/usr/share/man")

def build():
	autotools.make()

	pisitools.dosed("tclConfig.sh", "%s/unix" % a, "/usr/lib")
	pisitools.dosed("tclConfig.sh", "%s" % a, "/usr/include")

	pisitools.dosed("pkgs/tdbc1.1.9/tdbcConfig.sh", "%s/unix/pkgs/tdbc1.1.9" % a, "/usr/lib/tdbc1.1.9")
	pisitools.dosed("pkgs/tdbc1.1.9/tdbcConfig.sh", "%s/pkgs/tdbc1.1.9/generic" % a, "/usr/include")
	pisitools.dosed("pkgs/tdbc1.1.9/tdbcConfig.sh", "%s/pkgs/tdbc1.1.9/library" % a, "/usr/lib/tcl8.6")
	pisitools.dosed("pkgs/tdbc1.1.9/tdbcConfig.sh", "%s/pkgs/tdbc1.1.9" % a, "/usr/include")

	pisitools.dosed("pkgs/itcl4.3.0/itclConfig.sh", "%s/unix/pkgs/itcl4.3.0" % a, "/usr/lib/itcl4.3.0")
	pisitools.dosed("pkgs/itcl4.3.0/itclConfig.sh", "%s/pkgs/itcl4.3.0" % a, "/usr/include")

def check():
	autotools.make("test")

def install():
	autotools.rawInstall("DESTDIR=%s install-private-headers" % get.installDIR())

	pisitools.dosym("tclsh8.6", "/usr/bin/tclsh")
	pisitools.rename("/usr/share/man/man3/Thread.3", "Tcl_Thread.3")

	pisitools.insinto("/usr/share/copyright/tcl", "../license.terms")
