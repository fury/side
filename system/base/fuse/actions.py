#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, mesontools, get

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -D{examples,tests}=false '
    ])

def setup():
	shelltools.system("sed -i '/^udev/,$ s/^/#/' util/meson.build")
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	shelltools.system("chmod u+s %s/usr/bin/fusermount3" % get.installDIR())
