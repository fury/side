#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	autotools.rawConfigure("--prefix=/usr --mandir=/usr/share/man --use-libc")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for l in [".a", "-ffi.a"]:
		pisitools.remove("/usr/lib/liburing%s" % l)
