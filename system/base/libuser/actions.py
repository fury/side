#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' PYTHON=/usr/bin/python',
    ' --with-python',
    ' --without-sasl',
    ' --without-ldap',
    ' --disable-nls '
    ])

def setup():
	# disallow build documentations.
	pisitools.dosed("autogen.sh", "gtkdocize", deleteLine = True)
	pisitools.dosed("Makefile.am", "po\ docs", "po")
	pisitools.dosed("configure.ac", "GTK_DOC_CHECK", "")
	pisitools.dosed("configure.ac", "docs\/Makefile\ docs\/reference\/Makefile", "")

	shelltools.system("./autogen.sh")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
