#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

a = ''.join([
    ' -DSQLITE_ENABLE_COLUMN_METADATA=1',
    ' -DSQLITE_ENABLE_UNLOCK_NOTIFY=1',
    ' -DSQLITE_ENABLE_DBSTAT_VTAB=1 '
    ' -DSQLITE_SECURE_DELETE=1 '
    ])

i = ''.join([
    ' --prefix=/usr',
    ' --enable-fts{4,5}',
    ' CPPFLAGS="%s"' % a,
    ' --disable-static '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
