#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools, shelltools, get

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Di18n=disabled',
    ' -Ddb=auto',
    ])

def setup():
	pisitools.dosed("libpam/meson.build", "libintl", "")
	pisitools.dosed("meson_options.txt", "xsl-ns", "xsl")
	mesontools.configure(i)

def build():
	mesontools.build()

def check():
#	mesontools.build("test")
	pass

def install():
	mesontools.install()

	for f in ["system-session", "system-account", "system-password", "system-auth", "other"]:
		pisitools.doexe(f, "/etc/pam.d/")
	shelltools.chmod("%s/usr/sbin/unix_chkpwd" % get.installDIR(), mode = 04711)

	for l in ["/usr/lib/systemd", "/usr/share/doc"]:
		pisitools.removeDir(l)
