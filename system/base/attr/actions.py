#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --disable-{nls,static} '
    ])

def setup():
	pisitools.dosed("Makefile.in", "^dist_doc_DATA", deleteLine = True)
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for a in ["COPYING", "COPYING.LGPL"]:
		pisitools.insinto("/usr/share/copyright/%s" % get.srcNAME(), "doc/%s" % a)
