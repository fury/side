#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

def build():
	pisitools.dosed("src/Makefile", "install\ \-D\ \-m\ 644\ libaio.a", deleteLine = True)
	autotools.make()

def check():
	autotools.make("partcheck")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for i in shelltools.ls("man"):
		if i.endswith(".3"):
			pisitools.doman("man/%s" % i)
