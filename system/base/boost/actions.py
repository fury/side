#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, get, docopyright

i = ''.join([
    ' --prefix=%s/usr' % get.installDIR(),
    ' --with-toolset=gcc',
    ' --with-python=python3',
    ' --with-icu '
    ])

def setup():
	shelltools.system("./bootstrap.sh %s" % i)

def build():
	l = "cxxflags='%s' linkflags='%s'" % (get.CXXFLAGS(), get.LDFLAGS())
	shelltools.system("./b2 -d+2 %s link=shared threading=multi stage" % l)

def install():
	shelltools.system("./b2 link=shared threading=multi install")

	docopyright.installCopyright()
