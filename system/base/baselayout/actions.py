#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, get

def install():
	for l in open("rootfs", "r").readlines():
		i = l.strip()
		pisitools.dodir(i)

	pisitools.dosym(".", "/boot/boot")
	pisitools.dosym("/run", "/var/run")
	pisitools.dosym("/tmp", "/var/tmp")
	pisitools.dosym("/run/lock", "/var/lock")
	pisitools.dosym("/usr/lib/ld-linux-x86-64.so.2", "/lib64/ld-linux-x86-64.so.2")
	for sym in ["bin", "lib", "sbin"]:
		pisitools.dosym("/usr/%s" % sym, sym)

	def chmod(path, mode):
		shelltools.chmod("%s%s" % (get.installDIR(), path), mode)

	pisitools.dosed("etc/os-release", "@VERSION@", get.srcVERSION())
	pisitools.insinto("/etc", "etc/*")
	pisitools.insinto("/usr/share/etc", "usr/*")

	chmod("/root", 0700)
	chmod("/tmp", 01777)
	chmod("/usr/share/etc/shadow", 0600)
	for i in ["hourly", "daily", "weekly", "monthly"]:
		chmod("/etc/cron.%s" % i, 0754)
