#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get, docopyright

def build():
	shelltools.export("CCACHE_DIR", "%s" % get.workDIR())
	autotools.make()

def install():
	pisitools.dobin("disktype")
	pisitools.doman("disktype.1")

	docopyright.installCopyright()
