#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dpam_prefix=/etc/pam.d',
    ' -Dos_type=lfs',
    ' -Dman=true',
    ' -Dsession_tracking=elogind '
    ])

def setup():
	pisitools.cflags.add("-I/usr/include/elogind")
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	a = "localauthority"
	pisitools.insinto("/etc/polkit-1/%s" % a, "60-%s.conf" % a)

	for l in ["systemd", "sysusers.d", "tmpfiles.d"]:
		pisitools.removeDir("/usr/lib/%s" % l)
