#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' ADJTIME_PATH=/var/lib/hwclock/adjtime',
    ' --bindir=/usr/bin',
    ' --libdir=/usr/lib',
    ' --sbindir=/usr/sbin',
    ' --disable-chfn-chsh',
    ' --disable-login',
    ' --disable-nologin',
    ' --disable-su',
    ' --disable-setpriv',
    ' --disable-runuser',
    ' --disable-pylibmount',
    ' --disable-liblastlog2',
    ' --disable-static',
    ' --without-python',
    ' --without-btrfs',
    ' --without-udev',
    ' --without-systemd',
    ' --without-systemdsystemunitdir '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.dodir("/var/lib/hwclock")
