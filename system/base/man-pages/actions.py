#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def install():
	autotools.rawInstall("DESTDIR=%s prefix=/usr" % get.installDIR())

	# fix libxcrypt file conflicts
	for r in ["crypt.3", "crypt_r.3"]:
		pisitools.remove("/usr/share/man/man3/%s" % r)
