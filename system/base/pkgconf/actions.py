#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

def setup():
	pisitools.dosed("Makefile.am", "^dist_doc_DATA", deleteLine = True)
	shelltools.system("sh autogen.sh")
	autotools.configure("--prefix=/usr --disable-static")

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	pisitools.dosym("pkgconf", "/usr/bin/pkg-config")
