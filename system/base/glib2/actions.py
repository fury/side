#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, shelltools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Dintrospection=auto',
    ' -Dman-pages=enabled',
    ' -Dselinux=disabled '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()
	pisitools.removeDir("/usr/share/locale")
