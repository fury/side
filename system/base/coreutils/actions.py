#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, pisitools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --libexecdir=/usr/lib',
    ' --enable-single-binary=symlinks',
    ' --enable-single-binary-exceptions=""',
    ' --enable-no-install-program="kill,uptime"',
    ' --disable-nls '
    ])

def setup():
	autotools.autoreconf("-fiv")
	shelltools.export("FORCE_UNSAFE_CONFIGURE", "1")
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("check")
	pass

def install():
	for a in ["thanks-gen", "THANKS-to-translators", "THANKS.in", "THANKStt.in"]:
		shelltools.unlink(a)
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/etc", "src/dircolors.hin", "DIR_COLORS")
	pisitools.remove("/usr/bin/chroot")
	pisitools.dosym("/usr/bin/coreutils", "/usr/sbin/chroot")
