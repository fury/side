#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --with-acl',
    ' --with-btrfs=no',
    ' --with-libbsd=no',
    ' --with-group-name-max-length=32',
    ' --with-{b,yes}crypt',
    ' --disable-nls',
    ' --disable-static '
    ])

i__1 = [
       'FAIL_DELAY',
       'FAILLOG_ENAB',
       'LASTLOG_ENAB',
       'MAIL_CHECK_ENAB',
       'OBSCURE_CHECKS_ENAB',
       'PORTTIME_CHECKS_ENAB',
       'QUOTAS_ENAB',
       'CONSOLE',
       'MOTD_FILE',
       'FTMP_FILE',
       'NOLOGINS_FILE',
       'ENV_HZ',
       'PASS_MIN_LEN',
       'SU_WHEEL_ONLY',
       'CRACKLIB_DICTPATH',
       'PASS_CHANGE_TRIES',
       'PASS_ALWAYS_WARN',
       'CHFN_AUTH',
       'ENCRYPT_METHOD',
       'ENVIRON_FILE'
       ]

chld = [
       'chfn',
       'chsh',
       'chgpasswd',
       'groupadd',
       'groupdel',
       'groupmems',
       'groupmod',
       'useradd',
       'userdel',
       'usermod'
       ]

def setup():
	shelltools.system("sed -i 's/groups\$(EXEEXT) //' src/Makefile.in")
	shelltools.system("find man -name Makefile.in -exec sed -i 's/groups\.1 / /' {} \;")
	shelltools.system("find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;")
	shelltools.system("find man -name Makefile.in -exec sed -i 's/passwd\.5 / /' {} \;")
	shelltools.system("sed -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD YESCRYPT@' -i etc/login.defs")
	shelltools.system("sed -e '/PATH=/{s@/sbin:@@;s@/bin:@@}' -i etc/login.defs")
	shelltools.system("sed -e 's@/var/spool/mail@/var/mail@' -i etc/login.defs")
	shelltools.system("sed -i 's/^#HOME_MODE/HOME_MODE/' etc/login.defs")
	shelltools.system("sh shadow_file.sh")
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s exec_prefix=/usr" % get.installDIR())
	autotools.rawInstall("DESTDIR=%s -C man install-man" % get.installDIR())

	pisitools.insinto("/etc", "etc/login.defs", "login.defs.orig")
	pisitools.insinto("/etc/default", "useradd")

	for file_child in (tuple(chld)):
		pisitools.insinto("/etc/pam.d/", "chage", file_child)
		pisitools.dosed("%s/etc/pam.d/%s" % (get.installDIR(), file_child), "chage", file_child)

	for file_base in ["login", "passwd", "su", "chpasswd", "newusers", "chage"]:
		pisitools.doexe(file_base, "/etc/pam.d/")

	for t in (tuple(i__1)):
		pisitools.dosed("%s/etc/login.defs" % get.installDIR(), "^%s" % t, "# %s" % t)

	for t in ["limits", "login.access"]:
		pisitools.insinto("/etc", "etc/%s" % t, "%s.NOUSE" % t)
