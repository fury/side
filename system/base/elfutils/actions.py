#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --disable-nls',
    ' --disable-debuginfod',
    ' --enable-libdebuginfod=dummy '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("check")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for a in ["asm", "elf", "dw"]:
		pisitools.remove("/usr/lib/lib%s.a" % a)
