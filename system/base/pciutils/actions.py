#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' SHARED="yes"',
    ' PREFIX=/usr',
    ' IDSDIR=/usr/share/hwdata '
    ])

def build():
	autotools.make('OPT="%s" %s all' % (get.CFLAGS(), i))

def install():
	autotools.rawInstall('DESTDIR="%s" %s install-lib' % (get.installDIR(), i))
	pisitools.removeDir("/usr/share/hwdata")
