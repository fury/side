#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --with-readline=gnu '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make("all")

def check():
	autotools.make("check")

def install():
	autotools.rawInstall("DESTDIR=%s INST_LIB_DIR=%s/usr/lib" % (get.installDIR(), get.installDIR()))

	pisitools.removeDir("/usr/share/doc")
