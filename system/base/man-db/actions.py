#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --disable-setuid',
    ' --enable-cache-owner=bin',
    ' --with-browser=/usr/bin/lynx',
    ' --with-vgrind=/usr/bin/vgrind',
    ' --with-grap=/usr/bin/grap',
    ' --with-systemdtmpfilesdir=',
    ' --with-systemdsystemunitdir= '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("check")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.removeDir("/usr/share/doc")
