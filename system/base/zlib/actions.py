#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

i = ''.join([
    ' CFLAGS="%s"' % get.CFLAGS(),
    ' LDFLAGS="%s"' % get.LDFLAGS()
    ])

def setup():
	pisitools.dosed("Makefile.in", "\(LDCONFIG\)", deleteLine = True)
	autotools.rawConfigure("--prefix=/usr")

def build():
	autotools.make(i)

def check():
	autotools.make("check")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	pisitools.remove("/usr/lib/libz.a")
