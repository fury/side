#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import pisitools, autotools, get

i = ''.join([
    ' --prefix=/usr',
    ' --with-system-{libmpdec,expat}',
    ' --enable-{optimizations,shared}',
    ' --with-ensurepip=no '
    ])

def setup():
	autotools.rawConfigure(i)

def build():
	autotools.make()

def check():
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.remove("/usr/bin/2to3")
