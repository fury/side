#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc/ssh',
    ' --with-privsep-path=/var/lib/sshd',
    ' --with-default-path=/usr/bin',
    ' --with-superuser-path=/usr/sbin:/usr/bin',
    ' --with-pid-dir=/run',
    ' --with-{kerberos5,libedit,pam}',
    ' --with-ssl-engine',
    ' --with-mantype=man '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("-j1 tests")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.insinto("/etc/pam.d", "contrib/sshd.pam.generic", "sshd")
	pisitools.dobin("contrib/findssl.sh")

	pisitools.dobin("contrib/ssh-copy-id")
	pisitools.doman("contrib/ssh-copy-id.1")

	pisitools.insinto("/usr/share/copyright/openssh", "LICENCE")
