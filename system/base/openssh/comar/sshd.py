#!/usr/bin/python
# -*- coding: utf-8 -*-

from comar.service import *

serviceType = "server"
serviceDesc = _({"en": "Secure Shell Server"})

MSG_ERR_NEEDCONF = _({"en": "You need /etc/ssh/sshd_config to run sshd."})

PID_FILE = "/run/sshd.pid"
RSA_KEY = "/etc/ssh/ssh_host_rsa_key"
ECDSA_KEY = "/etc/ssh/ssh_host_ecdsa_key"
ED25519_KEY = "/etc/ssh/ssh_host_ed25519_key"

def check_config():
    import os
    if not os.path.exists("/etc/ssh/sshd_config"):
        fail(MSG_ERR_NEEDCONF)
    if not os.path.exists(ED25519_KEY):
        run("/usr/bin/ssh-keygen", "-t", "ed25519",
            "-f", "/etc/ssh/ssh_host_ed25519_key", "-N", "")
    if not os.path.exists(ECDSA_KEY):
        run("/usr/bin/ssh-keygen", "-t", "ecdsa",
            "-f", "/etc/ssh/ssh_host_ecdsa_key", "-N", "")
    if not os.path.exists(RSA_KEY):
        run("/usr/bin/ssh-keygen", "-t", "rsa",
            "-f", "/etc/ssh/ssh_host_rsa_key", "-N", "")

@synchronized
def start():
    check_config()
    startService(command="/usr/sbin/sshd",
                 args="-D",
                 pidfile=PID_FILE,
                 detach=True,
                 donotify=True)

@synchronized
def stop():
    stopService(pidfile=PID_FILE,
                donotify=True)

def status():
    return isServiceRunning(PID_FILE)
