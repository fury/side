#!/usr/bin/python2
# -*- coding: utf-8 -*-

from comar.service import *

serviceType = "local"
serviceDesc = _({"en": "System SSH Agent"})
serviceDefault = "on"

PID_FILE = "/run/ssh-agent.pid"

@synchronized
def start():
    startService(command="/usr/bin/ssh-agent",
                 args="-D -a /tmp/ssh-agent.socket",
                 pidfile=PID_FILE,
                 makepid=True,
                 detach=True,
                 donotify=True)

@synchronized
def stop():
    stopService(pidfile=PID_FILE,
                donotify=True)

def status():
    return isServiceRunning(PID_FILE)
