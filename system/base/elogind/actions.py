#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import mesontools, pisitools

i = ''.join([
    ' --prefix=/usr',
    ' --buildtype=release',
    ' -Drootprefix=/usr',
    ' -Dmode=release',
    ' -Dtranslations=false',
    ' -Dman=true',
    ' -Ddev-kvm-mode=0660',
    ' -Dpamlibdir=/usr/lib/security',
    ' -Drootlibdir=/usr/lib',
    ' -Ddbuspolicydir=/usr/share/dbus-1/system.d '
    ])

def setup():
	mesontools.configure(i)

def build():
	mesontools.build()

def install():
	mesontools.install()

	pisitools.removeDir("/usr/share/doc")
