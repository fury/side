#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --sysconfdir=/etc',
    ' --with-openssl',
    ' --with-{xz,zlib,zstd} '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
#	autotools.make("check")
	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	for s in ["depmod", "insmod", "modinfo", "modprobe", "rmmod"]:
		pisitools.dosym("/usr/bin/kmod", "/usr/sbin/%s" % s)
		pisitools.remove("/usr/bin/%s" % s)
