#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-unicode',
    ' --enable-jit',
    ' --enable-pcre2-16',
    ' --enable-pcre2-32',
    ' --enable-pcre2grep-libz',
    ' --enable-pcre2grep-libbz2',
    ' --enable-pcre2test-libreadline',
    ' --disable-static '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.removeDir("/usr/share/doc")
