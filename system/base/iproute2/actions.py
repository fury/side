#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, get

i = ''.join([
    ' PREFIX=/usr',
    ' MANDIR=/usr/share/man',
    ' NETNS_RUN_DIR=/run/netns',
    ' SBINDIR=/usr/sbin',
    ' DESTDIR=%s' % get.installDIR()
    ])

def setup():
	autotools.rawConfigure()

def build():
	autotools.make()

def install():
	autotools.rawInstall(i)
