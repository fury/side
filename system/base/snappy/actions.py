#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import cmaketools, mesontools

i = ''.join([
    ' -DCMAKE_BUILD_TYPE=Release',
    ' -DCMAKE_INSTALL_LIBDIR=lib',
    ' -DBUILD_SHARED_LIBS=ON',
    ' -DSNAPPY_BUILD_TESTS=OFF',
    ' -DSNAPPY_BUILD_BENCHMARKS=OFF',
    ' -Bbuild -G Ninja -L '
    ])

def setup():
	cmaketools.configure(i)

def build():
	mesontools.build()

def check():
	pass

def install():
	mesontools.install()
