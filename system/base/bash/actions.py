#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' bash_cv_strtold_broken=no',
    ' --without-bash-malloc',
    ' --with-installed-readline '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def check():
	autotools.make("check")
#	pass

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())
	pisitools.dosym("bash", "/usr/bin/sh")
	pisitools.dosym("bash", "/usr/bin/rbash")

	pisitools.removeDir("/usr/share/doc")
