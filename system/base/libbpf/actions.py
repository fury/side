#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

def setup():
	pisitools.dosed("src/Makefile", "\$\(STATIC_LIBS\)\ ", "")

def build():
	autotools.make("-C src")

def install():
	autotools.rawInstall("DESTDIR=%s -C src LIBSUBDIR=lib install_headers" % get.installDIR())
