#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import autotools, pisitools, get

i = ''.join([
    ' --prefix=/usr',
    ' --enable-cmdlib',
    ' --enable-pkgconfig',
    ' --enable-udev_sync',
    ' --enable-dmeventd',
    ' --with-default-pid-dir=/run',
    ' --with-tmpfilesdir=/usr/lib/tmpfiles.d',
    ' --with-modulesdir=/usr/lib/modules '
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()

def install():
	autotools.rawInstall("DESTDIR=%s" % get.installDIR())

	pisitools.remove("/usr/lib/udev/rules.d/69-dm-lvm.rules")
