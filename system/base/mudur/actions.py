#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, docopyright, pisitools, get

def install():
	shelltools.system("./setup.py install %s" % get.installDIR())

	for t in ["enabled", "disabled", "conditional"]:
		pisitools.dodir("/etc/mudur/services/%s" % t)

	docopyright.installCopyright()
