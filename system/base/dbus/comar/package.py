#!/usr/bin/python2
# -*- coding: utf-8 -*-

import os, re

def postInstall(fromVersion, fromRelease, toVersion, toRelease):
    if os.path.exists("/var/lib/dbus/machine-id"):
        try:
            os.symlink("/var/lib/dbus/machine-id", "/etc/machine-id")
        except:
            pass

def preRemove():
    try:
        os.unlink("/etc/machine-id")
    except:
        pass
