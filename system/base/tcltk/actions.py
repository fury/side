#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file https://www.gnu.org/licenses/gpl-3.0.txt

from pisi.actionsapi import shelltools, autotools, pisitools, get

WorkDir = "tk%s/unix" % get.srcVERSION()

i = ''.join([
    ' --prefix=/usr',
    ' --enable-64bit',
    ' --enable-man-symlinks',
    ' --disable-xss'
    ])

def setup():
	autotools.configure(i)

def build():
	autotools.make()
	# Removes the references to the build directory and replaces them with system-wide locations.
	shelltools.system("sed -i '/^TK/s/var.*unix/usr\/lib/g' tkConfig.sh")
	shelltools.system("sed -i '/^TK_SRC_DIR/s/var.*15/usr\/include/g' tkConfig.sh")

def install():
	for b in ["install", "install-private-headers"]:
		autotools.rawInstall("DESTDIR=%s %s" % (get.installDIR(), b))

	pisitools.insinto("/usr/share/copyright/tcltk", "license.terms")
	pisitools.dosym("wish8.6", "/usr/bin/wish")
